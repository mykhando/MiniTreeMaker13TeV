
#ifndef ANAHELPER_H
#define ANAHELPER_H

#include <iostream>
#include <string>
#include <vector>
#include <map>
#include <unordered_map>

#ifndef __MAKECINT__
#include "xAODJet/JetContainer.h"
#include "xAODMuon/MuonContainer.h"
#include "xAODEgamma/ElectronContainer.h"
#endif // not __MAKECINT__

#include "xAODEgamma/Electron.h"
#include "xAODEgamma/EgammaxAODHelpers.h"
#include "xAODCaloEvent/CaloCluster.h"
#include "xAODCaloEvent/CaloClusterContainer.h"

#include "xAODMuon/MuonAuxContainer.h"
#include "xAODJet/JetAuxContainer.h"
#include "xAODEgamma/ElectronAuxContainer.h"
#include "xAODMissingET/MissingETContainer.h"
#include "xAODMissingET/MissingETAuxContainer.h"
#include "xAODCutFlow/CutBookkeeper.h"
#include "xAODCutFlow/CutBookkeeperContainer.h"

#include "TFile.h"
#include "TString.h"
#include "TTree.h"
#include "TH1.h"
#include "MiniTreeMaker13TeV/MCtruth.h"
#include "MiniTreeMakerCommon.h"
class Anahelp{
  friend class MiniTreeMaker13TeV;
  friend class initfinal;
  public:
  Anahelp(){};
  virtual ~Anahelp(){};
  void fillTree(){t->Fill();};

  private:
  TFile *fout;
  bool m_isMC;
  TTree *t;
  // Histograms
  TH1F* m_cutFlow;
  //this is for powheg initial SoW
  unsigned int nprocessed;
  float wtgenPow;
  bool passTrigger_el;
  bool passTrigger_mu;
  //The containers with selected objects

  xAOD::MuonContainer* goodMuons; //!
  xAOD::MuonContainer* inclMuons; //!
  xAOD::ElectronContainer* goodElecs; //!
  xAOD::ElectronContainer* goodFwdElecs; //!
  xAOD::JetContainer* goodJets; //!
  xAOD::MuonAuxContainer *goodMuonsAux; //!
  xAOD::MuonAuxContainer *inclMuonsAux; //!
  xAOD::ElectronAuxContainer *goodElecsAux; //!
  xAOD::ElectronAuxContainer *goodFwdElecsAux; //!
  xAOD::JetAuxContainer* goodJetsAux; //!
  xAOD::MissingETContainer* MetContainer; //!
  xAOD::MissingETAuxContainer* MetAuxContainer; //!
  std::vector< bool > NomSelElecs;
  std::vector< bool > NomSelMuons;
  std::vector< bool > NomSelJets;
  MiniTreeMakerCommon *mini;

  public:

  ////////////////////////////////////
  //
  //
  ////////////////////////////////////
  void initialise(TString foutname, bool ismc){
    fout = new TFile(foutname, "RECREATE");
    m_isMC = ismc;
    mini = new MiniTreeMakerCommon();
    mini->initialise(m_isMC);
    t = mini->getTree();
    InitialiseCutflow();

    goodMuons = new xAOD::MuonContainer();
    inclMuons = new xAOD::MuonContainer();
    goodElecs = new xAOD::ElectronContainer();
    goodFwdElecs = new xAOD::ElectronContainer();
    goodJets = new xAOD::JetContainer();
    MetContainer    = new xAOD::MissingETContainer();

    goodMuonsAux = new xAOD::MuonAuxContainer();
    inclMuonsAux = new xAOD::MuonAuxContainer();
    goodElecsAux = new xAOD::ElectronAuxContainer();
    goodFwdElecsAux = new xAOD::ElectronAuxContainer();
    goodJetsAux = new xAOD::JetAuxContainer();
    MetAuxContainer = new xAOD::MissingETAuxContainer();

    goodMuons->setStore( goodMuonsAux );
    inclMuons->setStore( inclMuonsAux );
    goodElecs->setStore( goodElecsAux );
    goodFwdElecs->setStore( goodFwdElecsAux );
    goodJets->setStore( goodJetsAux );
    MetContainer->setStore( MetAuxContainer );

    //for Powheg sum of weights
    wtgenPow=0.;
    nprocessed = 0;
    return;
  }

  void getTruth(MCtruth* mctruth){
    std::pair<int, int> pdgid = mctruth->getPDGID();
    mini->tparton_pdgId1 = pdgid.first;
    mini->tparton_pdgId2 = pdgid.second;
    for(unsigned int i= 0; i < mctruth->tmet_source.size(); i++){
      (*mini->tu_pt)[mctruth->tmet_source[i]] = mctruth->tmet_pt[i];
      (*mini->tu_phi)[mctruth->tmet_source[i]] = mctruth->tmet_phi[i];
      (*mini->tu_sumet)[mctruth->tmet_source[i]] = mctruth->tmet_sumet[i];
    }
    if(mctruth->getNeutrino() != nullptr ){
      mini->tneutrino_pt->push_back( mctruth->getNeutrino()->pt());
      mini->tneutrino_e->push_back( mctruth->getNeutrino()->e());
      mini->tneutrino_eta->push_back( mctruth->getNeutrino()->eta());
      mini->tneutrino_phi->push_back( mctruth->getNeutrino()->phi());
      mini->tneutrino_pdgId->push_back( mctruth->getNeutrino()->pdgId());
    }
    mini->tboson_pt = mctruth->getBoson()->pt();
    mini->tboson_e = mctruth->getBoson()->e();
    mini->tboson_eta = mctruth->getBoson()->eta();
    mini->tboson_phi = mctruth->getBoson()->phi();
    mini->tboson_pdgId = mctruth->getBoson()->pdgId();

    for (auto& it : mctruth->getBareLeps()) {
      mini->tlep_bare_pt->push_back( (it)->pt());
      mini->tlep_bare_e->push_back( (it)->e());
      mini->tlep_bare_eta->push_back( (it)->eta());
      mini->tlep_bare_phi->push_back( (it)->phi());
      mini->tlep_bare_pdgId->push_back( (it)->pdgId());
    }
    for (auto& it : mctruth->getBornLeps()) {
      mini->tlep_born_pt->push_back( (it)->pt());
      mini->tlep_born_e->push_back( (it)->e());
      mini->tlep_born_eta->push_back( (it)->eta());
      mini->tlep_born_phi->push_back( (it)->phi());
      mini->tlep_born_pdgId->push_back( (it)->pdgId());
    }
    for (auto& it : mctruth->getDressedLeps()) {
      mini->tlep_dressed_pt->push_back( (it)->auxdata< float >( "pt_dressed" ));
      mini->tlep_dressed_e->push_back( (it)->auxdata< float >( "e_dressed" ));
      mini->tlep_dressed_eta->push_back( (it)->auxdata< float >( "eta_dressed" ));
      mini->tlep_dressed_phi->push_back( (it)->auxdata< float >( "phi_dressed" ));
      mini->tlep_dressed_pdgId->push_back( (it)->pdgId());
    }
    for (auto& it : mctruth->getPhotons()) {
      mini->tphoton_pt->push_back( (it)->pt());
      mini->tphoton_e->push_back( (it)->e());
      mini->tphoton_eta->push_back( (it)->eta());
      mini->tphoton_phi->push_back( (it)->phi());
    }
    for (auto& it : mctruth->getJets()) {
      mini->tjet_pt->push_back( (it)->pt());
      mini->tjet_e->push_back( (it)->e());
      mini->tjet_eta->push_back( (it)->eta());
      mini->tjet_phi->push_back( (it)->phi());
    }

  }
  void clearCont(){
    goodMuons->clear();
    inclMuons->clear();
    goodElecs->clear();
    goodFwdElecs->clear();
    goodJets->clear();
    MetContainer->clear();
  }
  void fillRecoilMap(TString u_name, TLorentzVector u){
    (*mini->u_pt)[u_name.Data()] = u.Pt();
    (*mini->u_phi)[u_name.Data()] = u.Phi();
    }
  // Initialise the cutflow
  void InitialiseCutflow() {
    m_cutFlow = new TH1F("CutFlow","Cut flow",50,0,50);

    m_cutFlow->GetXaxis()->SetBinLabel(1,"All events");
    m_cutFlow->GetXaxis()->SetBinLabel(2,"GRL");
    m_cutFlow->GetXaxis()->SetBinLabel(3,"LAr error");
    m_cutFlow->GetXaxis()->SetBinLabel(4,"PV");
    m_cutFlow->GetXaxis()->SetBinLabel(5,"Syst tools");
    m_cutFlow->GetXaxis()->SetBinLabel(6,">=1 good lepton");
    m_cutFlow->GetXaxis()->SetBinLabel(7,"el_channel or mu_channel");
    m_cutFlow->GetXaxis()->SetBinLabel(8,"pass trigger");
    m_cutFlow->GetXaxis()->SetBinLabel(40,"Initial Nevts");
    m_cutFlow->GetXaxis()->SetBinLabel(41,"Initial SoW");
    m_cutFlow->GetXaxis()->SetBinLabel(42,"Initial SoW2");

  }

  void AddCutflow(TFile *f , xAOD::TEvent& event, TString daodtype){

    //this is here for historical reasons, when producing personal derivations with specific format	
    //TH1F *h = (TH1F*) f->Get("CutFlow");
    TH1F *h = NULL;
    //simple case
    if(h != NULL){
      m_cutFlow -> Add(h);
    }
    else { //difficult case
      const xAOD::CutBookkeeperContainer* cutflows(0); 
      event.retrieveMetaInput(cutflows, "CutBookkeepers");
      // First, let's find the smallest cycle number,
      // i.e., the original first processing step/cycle
      TString tmps = daodtype + "Kernel";
      std::string derivationName = tmps.Data();
      int minCycle = 10000; 
      for (const xAOD::CutBookkeeper* cutflow: *cutflows) {
	if( !cutflow->name().empty() && minCycle > cutflow->cycle() ){ minCycle = cutflow->cycle(); }
      }
      // Now, let's actually find the right one that contains all the needed info...	
      const xAOD::CutBookkeeper* cutflowAll(0);
     // const xAOD::CutBookkeeper* cutflowDAOD(0);

      int maxCycle = -1; 
      for (const xAOD::CutBookkeeper* cutflow: *cutflows) {
	if( cutflow->inputStream() == "StreamAOD"&& cutflow->name() == "AllExecutedEvents" && maxCycle < cutflow->cycle() ){
	  maxCycle = cutflow->cycle();
	  cutflowAll = cutflow;
	}
	//if(cutflow->name() ==derivationName){
	//  cutflowDAOD = cutflow;  //not needed for now...maybe useful one day
	//}
      }
      uint64_t nEventsProcessed  = cutflowAll->nAcceptedEvents();
      //this is to avoid huge weights in Powheg due to xsection
      double SumOfWeights        = cutflowAll->sumOfEventWeights();
      double SumOfWeightsSquared = cutflowAll->sumOfEventWeightsSquared();
      m_cutFlow ->SetBinContent(40, nEventsProcessed +  m_cutFlow ->GetBinContent(40) );
      m_cutFlow ->SetBinContent(41,  SumOfWeights  +  m_cutFlow ->GetBinContent(41));
      m_cutFlow ->SetBinError(41,  sqrt(SumOfWeightsSquared+  (m_cutFlow ->GetBinError(41) * m_cutFlow->GetBinError(41))) );
    }
  }



  void WriteTree(bool isPowheg) {
    fout -> cd();
    if(isPowheg){
      m_cutFlow->SetBinContent(41, m_cutFlow ->GetBinContent(41) / (wtgenPow/nprocessed) );
      m_cutFlow->SetBinError(41, m_cutFlow ->GetBinError(41) / ( wtgenPow *wtgenPow/(nprocessed * nprocessed) ) );
      m_cutFlow->SetBinContent(42, m_cutFlow ->GetBinContent(42) / ( wtgenPow *wtgenPow/(nprocessed * nprocessed) ) );
    }
    m_cutFlow->Write();

    std::cout << "Writing Tree..." << std::endl;
    //fout -> cd();
    t->Write();
    fout->Close();
    delete fout;
    //delete m_cutFlow;
  }
  // Finalise
  // should be ~useless, TODO : merge with FinaliseSyst from initfinal
  void FinaliseEvent(xAOD::TEvent& m_event) {
    NomSelElecs.clear();
    NomSelMuons.clear();
    NomSelJets.clear();
    mini->clearVec();
    //	m_event.record( goodMuons, "goodMuons"  );
    //	m_event.record( goodElecs, "goodElecs"  );
    //	m_event.record( goodJets, "goodJets"  );
  }
  

};
#endif


#ifndef MET_H
#define MET_H

// xAOD ROOT access
#include "xAODRootAccess/Init.h"
#include "xAODRootAccess/TEvent.h"
#include "xAODRootAccess/TStore.h"
#include "xAODCore/ShallowCopy.h"
#include "xAODBase/IParticle.h"
#include "xAODBase/IParticleContainer.h"
#ifndef __MAKECINT__
#include "xAODJet/JetContainer.h"
#include "xAODMuon/MuonContainer.h"
#include "xAODEgamma/ElectronContainer.h"
#endif // not __MAKECINT__

#include "xAODMissingET/MissingETContainer.h"

#include "METUtilities/METMaker.h"
#include "METUtilities/CutsMETMaker.h"
#include "xAODMissingET/MissingETAssociationMap.h"
#include "HadronicRecoilPFO2016/hadrecoil.h"

#include "AsgTools/AnaToolHandle.h"
/*
namespace met {
    class METSystematicsTool;
    class METMaker;
}
*/
class MET {
    friend class initfinal;
    friend class MiniTreeMaker13TeV;
public:
    MET();
    virtual ~MET();
    //virtual void finalise();

    void InitialiseMETTools();
    int executeRecoil(xAOD::TEvent& m_event, xAOD::IParticleContainer* leptons);
    int getMET(xAOD::TEvent& m_event, xAOD::MissingETContainer *m_MetContainer, xAOD::ElectronContainer *goodElecs, xAOD::MuonContainer *goodMuons, xAOD::JetContainer *goodJets, float m_elecFinalMinPt, float m_muonFinalMinPt);

    TLorentzVector upfo(){return PFOHR;};
    TLorentzVector upfoEM(){return PFOHREM;};
    TLorentzVector upfovor(){return PFOVoronoiHR;};
    TLorentzVector upfoCharged(){return PFOHRCharged;};
    TLorentzVector upfovorsp(){return PFOVoronoiSpreadingHR;};
    TLorentzVector upfoNeutral(){return PFOHRNeutral;};
    TLorentzVector upfoNeutralEM(){return PFOHRNeutralEM;};
    double set(){return SET;};
    double nset(){return nSET;};
    TString m_METJetSelection;
    int u_ncones;
    

private:
    double SET, nSET;
    float m_cutPFOneutral;
    TLorentzVector PFOHR;
    TLorentzVector PFOVoronoiHR;
    TLorentzVector PFOVoronoiSpreadingHR;
    TLorentzVector PFOHREM , PFOHRCharged, PFOHRNeutral, PFOHRNeutralEM;

    asg::AnaToolHandle<IMETMaker> m_METMaker;
    hadrecoil *m_hadrecoil;
};
#endif




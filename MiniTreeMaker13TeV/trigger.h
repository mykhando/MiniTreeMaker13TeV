
#ifndef TRIGGER_H
#define TRIGGER_H

// xAOD ROOT access
#include "xAODRootAccess/Init.h"
#include "xAODRootAccess/TEvent.h"
#include "xAODRootAccess/TStore.h"
#include "xAODEventInfo/EventInfo.h"
#include "xAODCore/ShallowCopy.h"

#include "AsgTools/AnaToolHandle.h"
#include "TrigConfxAOD/xAODConfigTool.h"
#include "TrigDecisionTool/TrigDecisionTool.h"
#include "TriggerMatchingTool/MatchingTool.h"
#include "TriggerMatchingTool/IMatchingTool.h"

namespace TrigConf {
  class xAODConfigTool;
}
namespace Trig {
  class TrigDecisionTool;
}


class trigger {
  friend class MiniTreeMaker13TeV;
  friend class initfinal;
  friend class elecs;
  friend class muons;
  public:
  trigger();
  virtual ~trigger();
  //virtual void finalise();
  int getTriggerDecision();
  void InitialiseTriggerTools(bool ismc);
  TString m_TrigDecisionKey;

  std::vector<TString> m_passTriggerElecDatavec;
  std::vector<TString> m_passTriggerElecMCvec;
  std::vector<TString> m_passTriggerMuonDatavec;
  std::vector<TString> m_passTriggerMuonMCvec;
  bool passTrigger_el;
  bool passTrigger_mu;

  private:
  bool m_isMC;
  TrigConf::xAODConfigTool *m_confTool;
  Trig::TrigDecisionTool *m_tdt;
  asg::AnaToolHandle<Trig::IMatchingTool> m_elTrigMatch;	
  asg::AnaToolHandle<Trig::IMatchingTool> m_muTrigMatch;	

};
#endif


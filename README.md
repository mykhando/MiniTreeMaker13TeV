MiniTree maker to convert STDM4 DAODs to W mass MiniTree format


Setting up your area:
---------------------
	You should make 3 repositories in your analysis area :
	build  run  source
	###In principal the following is not needed anymore
	#Then you should also (in that repository) checkout atlasexternals
	#git clone https://:@gitlab.cern.ch:8443/akraszna/atlasexternals.git
	#From this you can copy the FastJet and FastJetContrib packages into your source directory :
	  #cp -r atlasexternals/External/FastJet* source/
	
Checkout dependencies:
----------------------
	inside the source repository you should get the HadronicRecoilPFO package (and rename it...this will have to be fixed one day somehow):
	  cd source
	  git clone https://:@gitlab.cern.ch:8443/fballi/HadronicRecoilPFO.git
	  mv HadronicRecoilPFO HadronicRecoilPFO2016

Checkout the package:
---------------------

	Do it still from inside the source repository.
	git clone --recursive https://:@gitlab.cern.ch:8443/atlas-wmass/MiniTreeMaker13TeV.git
	(the recursive is needed to checkout the Common files directory)
	if you did not use recursive you can do (inside the MiniTreeMaker13TeV folder):
	   git submodule update --init --recursive
	Now you have to copy a CMakeList file. From inside the source repository this is :
	cp MiniTreeMaker13TeV/CMakeLists_source.txt CMakeLists.txt
	
Compile and run:
----------------

      setupATLAS
      cd build
      asetup AnalysisBase,21.2.5	#this has to be done each time you log in (from  the build repository)
      cmake ../source	
      make				#compiling
      source */setup.sh			#this has to be done each time you log in (from  the build repository)
      
      cd ../run
      runWZ -i input_file1,inputfil2,... -c path_to_config_file [-n nEvents]
      

Information:
-----------

	An exemple config file can be found in share/ConfigContain/settings.config_13TeV_2017_lowmuMCtest
	Exemple input and output files will be provided soon

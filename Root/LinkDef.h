#ifndef MINITREEMAKER13TEV_LINKDEF_H
#define MINITREEMAKER13TEV_LINKDEF_H

#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;
#pragma link C++ nestedclass;
#pragma link C++ class std::vector< std::pair<float,std::string> >+;
#pragma link C++ class vector<map<string,float> >+;
#pragma link C++ class map<string,float>+;
 
// Declare the class(es) to generate dictionaries for:
#pragma link C++ class MiniTreeMaker13TeV+;

#endif // MINITREEMAKER13TEV_LINKDEF_H

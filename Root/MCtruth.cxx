
#ifndef MCTRUTH_CXX
#define MCTRUTH_CXX

#include "MiniTreeMaker13TeV/MCtruth.h"



// Constructor
MCtruth::MCtruth(bool old, bool Sherpa, bool Powheg, bool SkipDress) :
  oldFormat(old),
  isSherpa(Sherpa),
  neutrino(0),
  trueBoson(0),
  isPowheg(Powheg),
  skipDress(SkipDress)
{		
  return;
}
// Destructor
MCtruth::~MCtruth() {

}

int MCtruth::execute(xAOD::TEvent& m_event){
  // get the event information
  const xAOD::EventInfo* eventInfo = 0;
  if( m_event.retrieve( eventInfo, "EventInfo") == xAOD::TReturnCode::kFailure ) {
    std::cout << "Failed to get EventInfo!" << std::endl; 
    abort();
  }
  bool m_isMC = false;                                                                                                                                                               
  if ( eventInfo->eventType( xAOD::EventInfo::IS_SIMULATION ) )
    m_isMC = true;
  if(!m_isMC){
    std::cerr<< "This tool should not be used !!"<<std::endl;
    std::cerr<< "Only run this over MC  !!"<<std::endl;
    std::cerr<< "Exiting"<<std::endl;
    exit(10);
  }
  //Truth MET
  const xAOD::MissingETContainer* truthMetContainer = 0;


  if( !m_event.retrieve( truthMetContainer, "MET_Truth").isSuccess() ) {
    std::cout << "Failed to get MET_Truth!" << std::endl; 
    abort();
  }
  for (xAOD::MissingETContainer::const_iterator tmitr = truthMetContainer->begin(); tmitr != truthMetContainer->end(); ++tmitr){
    tmet_source.push_back( (*tmitr)->name() ) ;
    tmet_pt.push_back( (*tmitr)->met() );
    tmet_phi.push_back( (*tmitr)->phi() );
    tmet_sumet.push_back( (*tmitr)->sumet() );
  }

  //these are only in the derivations ...
  if(!skipDress){
    const xAOD::TruthParticleContainer* tmuons = 0;
    m_event.retrieve( tmuons, "TruthMuons" );

    const xAOD::TruthParticleContainer* telecs = 0;
    m_event.retrieve( telecs, "TruthElectrons" );
    //neutrino = nullptr;
    pdg1 =  -9999;
    pdg2 = -9999;
    xAOD::TruthParticleContainer::const_iterator tmuons_itr = tmuons->begin();
    xAOD::TruthParticleContainer::const_iterator tmuons_end = tmuons->end();
    //keep it like that right now, have to change it later
    for (; tmuons_itr != tmuons_end; ++tmuons_itr){
      //switch for ptag 237ff: classifierParticleXX	
      unsigned int mu_type = 0, mu_orig = 0;
      if(oldFormat){
	mu_type = (*tmuons_itr)->auxdata<uint>("particleType");
	mu_orig = (*tmuons_itr)->auxdata<uint>("particleOrigin");
      }else{
	mu_type = (*tmuons_itr)->auxdata<uint>("classifierParticleType");
	mu_orig = (*tmuons_itr)->auxdata<uint>("classifierParticleOrigin");  
      }  
      //Isolated muon coming from W or Z
      //See :
      //https://svnweb.cern.ch/trac/atlasoff/browser/PhysicsAnalysis/MCTruthClassifier/tags/MCTruthClassifier-00-01-42/MCTruthClassifier/MCTruthClassifierDefs.h
      if(mu_type==6 && ((mu_orig==12) || (mu_orig==13)) ){
	xAOD::TruthParticle* part = new xAOD::TruthParticle();
	part->makePrivateStore((**tmuons_itr));
	dressedLeps.push_back(part);
      }
    }
    xAOD::TruthParticleContainer::const_iterator telecs_itr = telecs->begin();
    xAOD::TruthParticleContainer::const_iterator telecs_end = telecs->end();
    for (; telecs_itr != telecs_end; ++telecs_itr){
      unsigned int el_type = 0, el_orig = 0;
      if(oldFormat){
	el_type = (*telecs_itr)->auxdata<uint>("particleType");
	el_orig = (*telecs_itr)->auxdata<uint>("particleOrigin");
      }else{
	el_type = (*telecs_itr)->auxdata<uint>("classifierParticleType");
	el_orig = (*telecs_itr)->auxdata<uint>("classifierParticleOrigin");
      }
      //isolated electrons from W/Z
      if(el_type==2 && (el_orig==13 || el_orig==12)){
	xAOD::TruthParticle* part = new xAOD::TruthParticle();
	part->makePrivateStore((**telecs_itr));
	dressedLeps.push_back(part);
      }
    }
    //prompt photons                                                                                                                                  
    const xAOD::TruthParticleContainer* tphotons = 0;
    m_event.retrieve( tphotons, "TruthPhotons" );
    xAOD::TruthParticleContainer::const_iterator tphotons_itr = tphotons->begin();
    xAOD::TruthParticleContainer::const_iterator tphotons_end = tphotons->end();
    for (; tphotons_itr != tphotons_end; ++tphotons_itr){
      unsigned int ph_type = 0, ph_orig = 0;
      if(oldFormat){
	ph_type = (*tphotons_itr)->auxdata<uint>("particleType");
	ph_orig = (*tphotons_itr)->auxdata<uint>("particleOrigin");
      }else{
	ph_type = (*tphotons_itr)->auxdata<uint>("classifierParticleType");
	ph_orig = (*tphotons_itr)->auxdata<uint>("classifierParticleOrigin");
      }
      //photons from FSR : 
      // Seems to be : 	coming from W/Z in Powheg and isolated ?
      //			labelled as FSR in Sherpa and non isolated ?
      //
      bool isFSR = false;
      if( (isSherpa && ph_orig==40) || (isPowheg && (ph_orig == 12 || ph_orig == 13) ) )isFSR = true;	
      if( (ph_type==14 || ph_type==15) && isFSR){
	xAOD::TruthParticle* part = new xAOD::TruthParticle();
	part->makePrivateStore((**tphotons_itr));
	truthPhotons.push_back(part);
      }
    }

    // get truth jet container                                                                                                                                     
    const xAOD::JetContainer* tjets = 0;
    m_event.retrieve( tjets, "AntiKt4TruthWZJets" );

    xAOD::JetContainer::const_iterator tjets_itr = tjets->begin();
    xAOD::JetContainer::const_iterator tjets_end = tjets->end();

    for (; tjets_itr != tjets_end; ++tjets_itr){
      xAOD::Jet* part = new xAOD::Jet();
      part->makePrivateStore((**tjets_itr));
      truthJets.push_back(part);
    }

  }
  //get W/Z       
  const xAOD::TruthEventContainer* truthE = 0;
  m_event.retrieve(truthE, "TruthEvents");

  // loop over truth events

  xAOD::TruthEventContainer::const_iterator truthE_itr = truthE->begin();
  xAOD::TruthEventContainer::const_iterator truthE_end = truthE->end();
  unsigned int nTE = 0;
  for (; truthE_itr != truthE_end; ++truthE_itr){
    nTE++;
    pdg1 = (*truthE_itr)->auxdata< int >( "PDGID1" );
    pdg2 = (*truthE_itr)->auxdata< int >( "PDGID2" );
    int nPart = (*truthE_itr)->nTruthParticles();
    for(int iPart = 0;iPart<nPart;iPart++){
      const xAOD::TruthParticle* particle = (*truthE_itr)->truthParticle(iPart);
      //int barcode = 0;
      int status = 0, pdgId = 9999;

      //if(particle)  barcode = particle->barcode();
      if(particle)  status = particle->status();
      if(particle)  pdgId = particle->pdgId();
      //else continue;

      //////////////////
      // Born leptons //
      //////////////////

      //To be used for Sherpa - apparently cannot find the bosons ?????
      if(particle && isSherpa){
	if(fabs(particle->pdgId()) == 11 || fabs(particle->pdgId()) == 13){
	  if(particle->status() == 11 ){
	    xAOD::TruthParticle* part = new xAOD::TruthParticle();
	    part->makePrivateStore((*particle));
	    bornLeps.push_back(part);
	  }
	  if(particle->status() == 3){						
	    xAOD::TruthParticle* part = new xAOD::TruthParticle();
	    part->makePrivateStore((*particle));
	    bareLeps.push_back(part);
	  }
	}
	if(fabs(particle->pdgId()) == 12 || fabs(particle->pdgId()) == 14){
	  if(particle->status() == 3 ){
	    neutrino = new xAOD::TruthParticle();
	    neutrino->makePrivateStore((*particle));
	  }
	}

      }
      if(pdgId==23 || pdgId==24 || pdgId == -24){
	//Powhrg boson status bare
	if(status != 62)continue;
	trueBoson=new xAOD::TruthParticle();
	trueBoson->makePrivateStore((*particle));
	for(unsigned int ip=0; ip < particle->nChildren();ip++){
	  if(fabs(particle->child(ip)->pdgId()) == 13 ||  fabs(particle->child(ip)->pdgId()) == 11){
	    if(particle->child(ip)->status() == 3){
	      xAOD::TruthParticle* part = new xAOD::TruthParticle();
	      part->makePrivateStore((*particle->child(ip)));
	      bornLeps.push_back(part);
	    }
	    if(particle->child(ip)->status() == 1){						
	      xAOD::TruthParticle* part = new xAOD::TruthParticle();
	      part->makePrivateStore((*particle->child(ip)));
	      bareLeps.push_back(part);
	    }
	  }

	  if(fabs(particle->child(ip)->pdgId()) == 12 ||  fabs(particle->child(ip)->pdgId()) == 14){
	    if(particle->child(ip)->status() == 1){	
	      neutrino = new xAOD::TruthParticle();
	      neutrino->makePrivateStore((*particle->child(ip)));
	    }
	  }
	}
	//Fill born leptons
	if( bornLeps.size() < 1 )
	  for(unsigned int ip=0; ip < particle->nChildren();ip++){
	    if(fabs(particle->child(ip)->pdgId()) == 13 ||  fabs(particle->child(ip)->pdgId()) == 11){
	      if(particle->child(ip)->status() == 1){
		xAOD::TruthParticle* part = new xAOD::TruthParticle();
		part->makePrivateStore((*particle->child(ip)));
		bornLeps.push_back(part);
	      }
	    }
	  }
	else if(pdgId==23 && bornLeps.size() < 2 ){
	  unsigned int iborn = 999;
	  float drmax = 0.;
	  for(unsigned int ip=0; ip < particle->nChildren();ip++)
	    if(fabs(particle->child(ip)->pdgId()) == 13 ||  fabs(particle->child(ip)->pdgId()) == 11){
	      if(particle->child(ip)->status() == 1){
		float eta1 = particle->child(ip)->eta();
		float eta2 = (bornLeps.at(0))->eta();
		float phi1 = particle->child(ip)->phi();
		float phi2 = (bornLeps.at(0))->phi();
		if( deltaR( eta1,  phi1,  eta2,  phi2) > drmax ){
		  drmax = deltaR( eta1,  phi1,  eta2,  phi2);
		  iborn = ip;
		}
	      }
	    }
	  if(iborn < 999){
	    xAOD::TruthParticle* part = new xAOD::TruthParticle();
	    part->makePrivateStore((*particle->child(iborn)));
	    bornLeps.push_back(part);
	  }
	}
      }
    }
  }
  //std::cout<<"nte = "<<nTE<<std::endl;
  return 0;
}


std::vector<xAOD::TruthParticle*>  MCtruth::getPhotons(){
  return truthPhotons;
}
std::vector<xAOD::Jet*>  MCtruth::getJets(){
  return truthJets;
}
xAOD::TruthParticle* MCtruth::getNeutrino(){
  return neutrino;
}

std::vector<xAOD::TruthParticle*>  MCtruth::getBornLeps(){
  return bornLeps;
}
std::vector<xAOD::TruthParticle*>  MCtruth::getBareLeps(){
  return bareLeps;
}
std::vector<xAOD::TruthParticle*>  MCtruth::getDressedLeps(){
  return dressedLeps;
}
xAOD::TruthParticle* MCtruth::getBoson(){
  return trueBoson;
}


std::pair<int, int> MCtruth::getPDGID(){
  std::pair<int, int> pdgid(0, 0);
  pdgid.first = pdg1;
  pdgid.second = pdg2;
  return pdgid;
}


void MCtruth::finalise(){
  for(auto _t : truthPhotons)
    delete _t;
  for(auto _t : truthJets)
    delete _t;
  for(auto _t : bornLeps)
    delete _t;
  for(auto _t : bareLeps)
    delete _t;
  for(auto _t : dressedLeps)
    delete _t;

  truthPhotons.clear();
  truthJets.clear();
  bornLeps.clear();
  bareLeps.clear();
  dressedLeps.clear();

  if(neutrino) { delete neutrino; neutrino = 0;}
  if(trueBoson) {delete trueBoson; trueBoson = 0;}

}


#endif

#ifndef INITFINAL_CXX
#define INITFINAL_CXX

#include "MiniTreeMaker13TeV/initfinal.h"

template <class T>
void deleteStlVector(T *container) {
  typename T::const_iterator itr = container->begin();
  typename T::const_iterator itr_end = container->end();
  for(;itr!=itr_end;++itr) { if(*itr) delete (*itr); }
  //delete container;
}

// Template instantiations needed by g++
template void deleteStlVector(xAOD::ElectronContainer *container);
template void deleteStlVector(xAOD::JetContainer *container);
//template void deleteStlVector<MCParticle*>(std::vector<MCParticle*> *container);
template void deleteStlVector(xAOD::MuonContainer *container);
template void deleteStlVector(xAOD::IParticleContainer *container);

std::vector<TString> vectorise(TString str, TString sep=" ") {
  std::vector<TString> result; TObjArray *strings = str.Tokenize(sep.Data());
  if (strings->GetEntries()==0) { delete strings; return result; }
  TIter istr(strings);
  while (TObjString* os=(TObjString*)istr()) result.push_back(os->GetString());
  delete strings; return result;
}

// Constructor
initfinal::initfinal(void) : 
  AsgMessaging("MiniTreeMaker13TeV"){		
    return;
  }
// Destructor
initfinal::~initfinal() {

}

void initfinal::Initialise(xAOD::TEvent& event, std::string f_init, TString daod_type) {
  m_daod=daod_type;
  conf_file=f_init;
  ATH_MSG_INFO("++++++++++ Initialising MiniTreeMaker13TeV ++++++++++\n");
  // get path to RootCoreBin
  jettool = new jets();
  mettool = new MET();
  trigtool = new trigger();
  eltool = new elecs();
  mutool = new muons();
  initfinal::read(conf_file);
  eltool->m_antiIso = m_antiIso;
  mutool->m_antiIso = m_antiIso;
  mutool->m_do5TeV = m_do5TeV;
  //setting derivation-dependent booleans
  m_STDM = false;
  if( m_daod.Contains("STDM") )m_STDM = true;

  if(m_doTruth && !m_STDM){
    std::cerr << "WARNING : doTruth should only set to true for STDM MC samples !" <<std::endl;
    std::cerr << "WARNING : No truth info will be retrieved !" <<std::endl;
    m_doTruth = false;
  }
  if(m_forceTruth){
    std::cerr << "WARNING : force truth flag enabled !" <<std::endl;
    std::cerr << "WARNING : should be used on xAOD MCs!" <<std::endl;
    std::cerr << "WARNING : Skipping dressed leptons!" <<std::endl;
    m_doTruth = true;
  }

  // get the event information
  const xAOD::EventInfo* eventInfo = 0;
  //  if( !event.retrieve( eventInfo, "EventInfo").isSuccess() ) {
  if( event.retrieve( eventInfo, "EventInfo") == xAOD::TReturnCode::kFailure ) {
    std::cout << "Failed to get EventInfo!" << std::endl; 
    abort();
  }
  else
    ATH_MSG_INFO("+++++++ Successfully retrieved the event info.");

  // check if input dataset is MC
  m_isMC = false;                                                                                                                                                               
  if ( eventInfo->eventType( xAOD::EventInfo::IS_SIMULATION ) )
    m_isMC = true;
  ATH_MSG_INFO("+++++++ isMC? " << m_isMC );

  // if MC, determine which dataset and jet slice we're running over
  if (m_isMC) {
    m_mcChannelNumber = eventInfo->mcChannelNumber();
    ATH_MSG_INFO("++++ mc_channel_number = " << m_mcChannelNumber );
    if(Generator(m_mcChannelNumber).Contains("Powheg")){isPowheg = true;isSherpa = false;}
    else {isSherpa = true;isPowheg = false;}//have to code sherpa in Generator at some point
  }
  //first bool is isOldFormat for truth - may add a this accessible in config file later  -- TODO
  mctruth = new MCtruth(false, isSherpa, isPowheg, m_forceTruth);
  // switch on sumw2 for all histograms
  TH1::SetDefaultSumw2(kTRUE);

  // Initialise the tools, cutflow, histograms
  ATH_MSG_INFO("+++++++ Initialising tools \n");
  InitialiseGRL();
  InitialisePRW();
  eltool->InitialiseElecTools(m_isMC);
  mutool->InitialiseMuonTools(m_isMC);
  jettool->InitialiseJetTools(m_isMC);
  mettool->InitialiseMETTools();
  trigtool->InitialiseTriggerTools(m_isMC);


  //evtNbr=0;

  //this is also where histograms are initialised !
  ATH_MSG_INFO("+++++++ Initialising systematics \n");
  InitSystVariation();
  ATH_MSG_INFO("+++++++ Initialising AnaHelper \n");
  ana = new Anahelp();
  ana -> initialise(outName, m_isMC);
  ATH_MSG_INFO("++++++++++ End of initialising MiniTreeMaker13TeV ++++\n");

  return;
}

TString initfinal::Generator(int mcChannelNumber){
  TString t="";
  if(mcChannelNumber >= 361100 && mcChannelNumber <= 361108)//  W/Z
    t="Powheg";
  if(mcChannelNumber >= 410011 && mcChannelNumber <= 410014)//  single top
    t="Powheg";
  if(mcChannelNumber ==41000)//  ttbar
    t="Powheg";
  //At the moment all this looks useless but we have to add other generetors like Sherpa	
  return t;
}


void initfinal::read(std::string settings_file){

  m_rootCoreBin = std::string(getenv("UserAnalysis_DIR")); 

  if(settings_file =="")settings_file = m_rootCoreBin + "/../../source/MiniTreeMaker13TeV/share/settings.config";
  std::cout << "Reading settings file " << settings_file << std::endl;
  TEnv *m_settings = new TEnv();
  m_settings->ReadFile(settings_file.c_str(),EEnvLevel(0));

  // read config for analysis parameters
  m_DEBUG 		= m_settings->GetValue("DEBUG",false);
  m_pureweighting 		= m_settings->GetValue("PileUpReweighting",0);
  m_sysname 			= m_settings->GetValue("Systematic","");
  m_dosysEL 			= m_settings->GetValue("DoSystematicElecs",false);
  m_dosysMU 			= m_settings->GetValue("DoSystematicMuons",false);
  m_dosys = ( m_dosysEL || m_dosysMU );
  m_doTruth 			= m_settings->GetValue("DoTruth",true);
  m_forceTruth 			= m_settings->GetValue("ForceTruth",false);
  m_PassThrough 		= m_settings->GetValue("PassThrough",false);
  m_doAcc 			= m_settings->GetValue("DoAcceptance",false);
  m_antiIso 			= m_settings->GetValue("DoAntiIso",false);
  m_do5TeV 			= m_settings->GetValue("Do5TeV",false);
  m_doJpsi			= m_settings->GetValue("DoJpsi",false);

  // read config for lepton preselection cuts
  mutool->m_muonMaxEta  			= m_settings->GetValue("MuonMaxEta",2.4); 
  mutool->m_muonQuality 			= m_settings->GetValue("MuonQuality",1);
  mutool->m_muonMinPt   			= m_settings->GetValue("MuonMinPt",20000.);  //recoil muons, second muon veto (Z background reduction)
  mutool->m_muonMinPtIncl   			= m_settings->GetValue("MuonMinPtIncl",5000.);  //soft muons to be added to MET calculation
  mutool->m_muonFinalMinPt   		= m_settings->GetValue("MuonFinalMinPt",24000.); // just to cut on Nleptons==1, set a bit loose (e.g. 1 GeV less)
  m_muonTrigMatch	   		= m_settings->GetValue("MuonTrigMatch","HLT_mu20_iloose_L1MU15,HLT_mu50");
  mutool->m_muonMCTrigMatch  		= m_settings->GetValue("MuonMCTrigMatch","HLT_mu20_iloose_L1MU15_OR_HLT_mu50");
  mutool->muontrigmatchnames 		= vectorise(TString(m_muonTrigMatch),",");

  eltool->m_elecMaxEta      		= m_settings->GetValue("ElecMaxEta",2.47);	
  eltool->m_elecMinPt      		= m_settings->GetValue("ElecMinPt",15000.);	
  eltool->m_elecMaxFwdEta      		= m_settings->GetValue("ElecMaxFwdEta",4.9);	
  eltool->m_elecMinFwdPt      		= m_settings->GetValue("ElecMinFwdPt",0.);	
  eltool->m_elecMinEtaCrack 		= m_settings->GetValue("ElecMinEtaCrack",1.37);
  eltool->m_elecMaxEtaCrack 		= m_settings->GetValue("ElecMaxEtaCrack",1.52);
  eltool->m_elecFinalMinPt   		= m_settings->GetValue("ElecFinalMinPt",25000.);  
  m_elecTrigMatch	   		= m_settings->GetValue("ElecTrigMatch","HLT_e60_lhmedium,HLT_e24_lhmedium_L1EM20VH,HLT_e120_lhloose");
  m_elecMCTrigMatch  		= m_settings->GetValue("ElecMCTrigMatch","HLT_e60_lhmedium,HLT_e24_lhmedium_L1EM18VH,HLT_e120_lhloose");
  eltool->electrigmatchnames 		= vectorise(TString(m_elecTrigMatch),",");
  eltool->elecmctrigmatchnames 		= vectorise(TString(m_elecMCTrigMatch),",");
  eltool->m_elecPtMaxTrig			= m_settings->GetValue("ElecPtMaxTrig",150000.);
  if(m_doJpsi){
    mutool->m_muonFinalMinPt   		= 2000.; //J/Psi : no cut on the final lepton
    eltool->m_elecFinalMinPt   		= 2000.;  
    eltool->m_elecMinPt  		= 2000.;
    mutool->m_muonMinPt			= 2000.;
    mutool->m_muonMinPtIncl		= 2000.;
    }
  //recoil lepton veto
  m_elecRecoilMedium			= m_settings->GetValue("RecoilElecAtLeastMedium", true);
  m_elecRecoilTight			= m_settings->GetValue("RecoilElecAtLeastTight", false);
  m_muonRecoilTight			= m_settings->GetValue("RecoilMuonAtLeastTight", false);
  m_muonMinRecoilPt			= m_settings->GetValue("MuonMinRecoilPt", 18000.);
  m_elecMinRecoilPt			= m_settings->GetValue("ElecMinRecoilPt", 18000.);
  
   // read config for GRL and PU initialisation
  m_LumiCalcFile			= m_settings->GetValue("LumiCalcFile","/../MiniTreeMaker13TeV/share/ilumicalc_histograms_None_276262-284484.root");	      
  m_PUConfFile			= m_settings->GetValue("PUConfFile","/../MiniTreeMaker13TeV/share/mc15_13TeV.361106.PowhegPythia8EvtGen_AZNLOCTEQ6L1_Zee.merge.AOD.e3601_s2576_s2132_r7267_r6282_PRWmc15b.root,/../MiniTreeMaker13TeV/share/mc15_13TeV.361107.PowhegPythia8EvtGen_AZNLOCTEQ6L1_Zmumu.merge.AOD.e3601_s2576_s2132_r7267_r6282_PRWmc15b.root");	      
  m_vecLumiCalcFile		= vectorise(TString(m_LumiCalcFile),",");			   
  m_vecPUConfFile			= vectorise(TString(m_PUConfFile),",");	
  m_PUUnrepresentedDataAction	= m_settings->GetValue("PUUnrepresentedDataAction",2);
  m_PUDataScaleFactor		= m_settings->GetValue("PUDataScaleFactor",1/1.16);   
  m_PUDefaultChannel		= m_settings->GetValue("PUDefaultChannel",361106);      

  m_GRLlist			= m_settings->GetValue("GRLlist","/../MiniTreeMaker13TeV/share/data15_13TeV.periodAllYear_DetStatus-v73-pro19-08_DQDefects-00-01-02_PHYS_StandardGRL_All_Good_25ns.xml");		
  m_GRLVec 			= vectorise(TString(m_GRLlist),",");
  m_GRLPassThrough		= m_settings->GetValue("GRLPassThrough",false);

  // read config for jet tools intitialisation
  jettool -> m_JetAlgo			= m_settings->GetValue("JetAlgo","AntiKt4EMTopo");
  jettool -> m_JetConfig			= m_settings->GetValue("JetConfig","JES_MC15Prerecommendation_April2015.config");
  jettool -> m_JetCalibSeq			= m_settings->GetValue("JetCalibSeq","JetArea_Residual_EtaJES_GSC");
  jettool -> m_JetCleaningCutLevel		= m_settings->GetValue("JetCleaningCutLevel","LooseBad");
  jettool -> m_JetCleaningDoUgly		= m_settings->GetValue("JetCleaningDoUgly",false);
  jettool -> m_JERPlotFileName		= m_settings->GetValue("JERPlotFileName","JetResolution/Prerec2015_xCalib_2012JER_ReducedTo9NP_Plots_v2.root");
  jettool -> m_JERCollectionName		= m_settings->GetValue("JERCollectionName","AntiKt4EMTopoJets");
  jettool -> m_JetSmearingApplyNominal	= m_settings->GetValue("JetSmearingApplyNominal",false);
  jettool -> m_JetSmearingSystematicMode	= m_settings->GetValue("JetSmearingSystematicMode","Full");
  jettool -> m_JesUncerJetDefinition		= m_settings->GetValue("JesUncerJetDefinition","AntiKt4EMTopo");
  jettool -> m_JesUncerMCType		= m_settings->GetValue("JesUncerMCType","MC15");
  jettool -> m_JesUncerConfigFile		= m_settings->GetValue("JesUncerConfigFile","JES_2015/Prerec/PrerecJES2015_AllNuisanceParameters_25ns.config");

  // read config for muon tools intitialisation
  mutool -> m_MuToolTTVAWP 	  		= m_settings->GetValue("MuToolTTVAWP","TTVA"); 
  mutool -> m_MuToolTTVACalib	  	= m_settings->GetValue("MuToolTTVACalib","Data15_allPeriods_260116"); 
  mutool -> m_MuToolEffiWP 	  		= m_settings->GetValue("MuToolEffiWP","Medium"); 
  mutool -> m_MuToolEffiCalib	  	= m_settings->GetValue("MuToolEffiCalib","Data15_allPeriods_260116"); 
  mutool -> m_MuToolIsoWP  	  		= m_settings->GetValue("MuToolIsoWP","GradientIso"); 
  mutool -> m_MuToolCalibAndSmearStatComb    	= m_settings->GetValue("MuToolCalibAndSmearStatComb",false); 
  mutool -> m_MuToolTrigEffMuQual    	= m_settings->GetValue("MuToolTrigEffMuQual","Medium");
  mutool -> m_MuToolCalibAndSmearYear	= m_settings->GetValue("MuToolCalibAndSmearYear","Data15");
  mutool -> m_MuToolCalibAndSmearAlgo	= m_settings->GetValue("MuToolCalibAndSmearAlgo","muons");
  mutool -> m_MuToolCalibAndSmearType	= m_settings->GetValue("MuToolCalibAndSmearType","q_pT");
  mutool -> m_MuToolCalibAndSmearRel 	= m_settings->GetValue("MuToolCalibAndSmearRel","Recs2016_01_19");
  mutool -> m_MuToolCalibAndSmearSagittaCorr = m_settings->GetValue("MuToolCalibAndSmearSagittaCorr",true);
  mutool -> m_MuToolCalibAndSmearSagittaRel = m_settings->GetValue("MuToolCalibAndSmearSagittaRel","sagittaBiasDataAll_25_07_17");
  mutool -> m_MuToolCalibAndSmearSagittaMCDist = m_settings->GetValue("MuToolCalibAndSmearSagittaMCDist",false);
  
  // read config for elec tools intitialisation
  eltool->EL_CorrModel				= m_settings->GetValue("ElToolCorrModel", "FULL");
  std::string m_ElToolRecoCorrFile		= m_settings->GetValue("ElToolRecoCorrFile","ElectronEfficiencyCorrection/efficiencySF.offline.RecoTrk.2015.13TeV.rel20p0.25ns.v04.root");
  std::string m_ElToolLLHCorrFile_loose  = m_settings->GetValue("ElToolLLHCorrFile_loose","ElectronEfficiencyCorrection/efficiencySF.offline.LooseLLH_d0z0.2015.13TeV.rel20p0.25ns.v04.root");
  //std::string m_ElToolIsoCorrFile_loose  = m_settings->GetValue("ElToolIsoCorrFile_loose","ElectronEfficiencyCorrection/efficiencySF.Isolation.LooseLLH_d0z0_v8_isolGradient.2015.13TeV.rel20p0.25ns.v04.root");

  std::string m_ElToolLLHCorrFile_medium	= m_settings->GetValue("ElToolLLHCorrFile_medium","ElectronEfficiencyCorrection/efficiencySF.offline.MediumLLH_d0z0.2015.13TeV.rel20p0.25ns.v04.root");
  std::string m_ElToolIsoCorrFile_medium	= m_settings->GetValue("ElToolIsoCorrFile_medium","ElectronEfficiencyCorrection/efficiencySF.Isolation.MediumLLH_d0z0_v8_isolGradient.2015.13TeV.rel20p0.25ns.v04.root");
  std::string m_ElToolLLHCorrFile_tight		= m_settings->GetValue("ElToolLLHCorrFile_tight","ElectronEfficiencyCorrection/efficiencySF.offline.TightLLH_d0z0.2015.13TeV.rel20p0.25ns.v04.root");
  std::string m_ElToolIsoCorrFile_tight		= m_settings->GetValue("ElToolIsoCorrFile_tight","ElectronEfficiencyCorrection/efficiencySF.Isolation.TightLLH_d0z0_v8_isolGradient.2015.13TeV.rel20p0.25ns.v04.root");
  std::string m_ElToolTrigCorrFile		= m_settings->GetValue("ElToolTrigCorrFile","ElectronEfficiencyCorrection/efficiencySF.e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose.TightLLH_d0z0_v8_isolGradient.2015.13TeV.rel20p0.25ns.v04.root");
  std::string m_ElToolTrigEffCorrFile		= m_settings->GetValue("ElToolTrigEffCorrFile","ElectronEfficiencyCorrection/efficiency.e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose.TightLLH_d0z0_v8_isolGradient.2015.13TeV.rel20p0.25ns.v04.root");
  std::string ElToolIsoVetoCorrFile_default = std::string(m_rootCoreBin + "/../../source/MiniTreeMaker13TeV/share/efficiencySF.Isolation.TightLLH_d0z0_v11_isolFixedCutLoose.EFFICIENCY.root");
  std::string m_ElToolIsoVetoCorrFile		= m_settings->GetValue("ElToolIsoVetoCorrFile",ElToolIsoVetoCorrFile_default.c_str());
  eltool->m_ElToolRecoCorrFilevec		= vectorise(TString(m_ElToolRecoCorrFile),",");
  eltool->m_ElToolLLHCorrFilevec_loose    = vectorise(TString(m_ElToolLLHCorrFile_loose),",");
  //eltool->m_ElToolIsoCorrFilevec_loose   = vectorise(TString(m_ElToolIsoCorrFile_loose),",");
  eltool->m_ElToolLLHCorrFilevec_medium		= vectorise(TString(m_ElToolLLHCorrFile_medium),",");
  eltool->m_ElToolIsoCorrFilevec_medium		= vectorise(TString(m_ElToolIsoCorrFile_medium),",");
  eltool->m_ElToolLLHCorrFilevec_tight		= vectorise(TString(m_ElToolLLHCorrFile_tight),",");
  eltool->m_ElToolIsoCorrFilevec_tight		= vectorise(TString(m_ElToolIsoCorrFile_tight),",");
  eltool->m_ElToolIsoVetoCorrFilevec		= vectorise(TString(m_ElToolIsoVetoCorrFile),",");
  eltool->m_ElToolTrigCorrFilevec		= vectorise(TString(m_ElToolTrigCorrFile),",");
  eltool->m_ElToolTrigEffCorrFilevec	= vectorise(TString(m_ElToolTrigEffCorrFile),",");
  eltool->m_ElToolForcedataType		= m_settings->GetValue("ElToolForcedataType",1);
  eltool->m_ElToolCalibAndSmearESModel	= m_settings->GetValue("ElToolCalibAndSmearESModel","es2015PRE");
  eltool->m_ElToolCalibAndSmearDecorrModel= m_settings->GetValue("ElToolCalibAndSmearDecorrModel","FULL_v1");
  eltool->m_ElToolLlhPVCont		= m_settings->GetValue("ElToolLlhPVCont","PrimaryVertices");
  eltool->m_ElToolLlhLooseConfigFile	 = m_settings->GetValue("ElToolLlhLooseConfigFile","ElectronPhotonSelectorTools/offline/mc15_20150712/ElectronLikelihoodLooseOfflineConfig2015.conf");
  eltool->m_ElToolLlhMediumConfigFile		= m_settings->GetValue("ElToolLlhMediumConfigFile","ElectronPhotonSelectorTools/offline/mc15_20150712/ElectronLikelihoodTightOfflineConfig2015.conf");
  eltool->m_ElToolLlhTightConfigFile		= m_settings->GetValue("ElToolLlhTightConfigFile","ElectronPhotonSelectorTools/offline/mc15_20150712/ElectronLikelihoodMediumOfflineConfig2015.conf");
  eltool->m_ElFwdIDConfigFile		= m_settings->GetValue("ElFwdIDConfigFile","ElectronPhotonSelectorTools/offline/mc15_20150812/ForwardElectronIsEMTightSelectorCutDefs.conf");

  // read config for isolation tool for both el and mu initialisation 
  mutool -> m_IsolToolMuonWP = m_settings->GetValue("IsolToolMuonWP","Gradient");
  eltool->m_IsolToolElecWP = m_settings->GetValue("IsolToolElecWP","Gradient");

  // read config for trigger decision tool initialisation
  trigtool-> m_TrigDecisionKey = m_settings->GetValue("TrigDecisionKey","xTrigDecision");

  m_passTriggerElecData	= m_settings->GetValue("passTriggerElecData","HLT_e24_lhmedium_L1EM20VH,HLT_e60_lhmedium,HLT_e120_lhloose");
  trigtool-> m_passTriggerElecDatavec= vectorise(TString(m_passTriggerElecData),",");
  m_passTriggerElecMC	= m_settings->GetValue("passTriggerElecMC","HLT_e24_lhmedium_L1EM18VH,HLT_e60_lhmedium,HLT_e120_lhloose");
  trigtool-> m_passTriggerElecMCvec	= vectorise(TString(m_passTriggerElecMC),",");
  m_passTriggerMuonData	= m_settings->GetValue("passTriggerMuonData","HLT_mu20_iloose_L1MU15,HLT_mu50");
  trigtool-> m_passTriggerMuonDatavec= vectorise(TString(m_passTriggerMuonData),",");
  m_passTriggerMuonMC	= m_settings->GetValue("passTriggerMuonMC","HLT_mu20_iloose_L1MU15,HLT_mu50");
  trigtool-> m_passTriggerMuonMCvec	= vectorise(TString(m_passTriggerMuonMC),",");

  // read config for MET tool initialisation
  mettool->m_METJetSelection = m_settings->GetValue("METJetSelection","Tight");
  mettool->m_cutPFOneutral   = m_settings->GetValue("NeutralPFOCut",0.);
  
  // output file 
  outName =  m_settings->GetValue("fileOutName","outTree.root");
  // print
  if(m_settings->GetValue("DumpConfigSettings",1))
    initfinal::Print();
  delete m_settings;

  return;
}


void initfinal::Print(){
  ATH_MSG_INFO(" \n");
  ATH_MSG_INFO("+++++++++++++++++++++++++++++++++++++++++++++++++ ");
  ATH_MSG_INFO("++++ Dump config file ");
  ATH_MSG_INFO("+++++++++++++++++++++++++++++++++++++++++++++++++ ");
  ATH_MSG_INFO("++++ General analysis parameters ");
  ATH_MSG_INFO("++ PileUpReweighting " << m_pureweighting);
  ATH_MSG_INFO("++ Systematics       " << m_sysname);
  ATH_MSG_INFO("++ Elecs Systematics use   " << m_dosysEL);
  ATH_MSG_INFO("++ Muons Systematics use   " << m_dosysMU);
  ATH_MSG_INFO("++ Truth use         " << m_doTruth);
  ATH_MSG_INFO("+++++++++++++++++++++++++++++++++++++++++++++++++ ");
  ATH_MSG_INFO("++++ Lepton preselection cuts ");
  ATH_MSG_INFO("++ Muons ");
  ATH_MSG_INFO("++ MuonMaxEta        " << mutool -> m_muonMaxEta);		 
  ATH_MSG_INFO("++ MuonQuality       " << mutool -> m_muonQuality);		 
  ATH_MSG_INFO("++ MuonMinPt         " << mutool -> m_muonMinPt);		 
  ATH_MSG_INFO("++ MuonFinalMinPt    " << mutool -> m_muonFinalMinPt); 	 
  ATH_MSG_INFO("++ MuonTrigMatch     " << m_muonTrigMatch);  	 
  ATH_MSG_INFO("++ MuonMCTrigMatch   " << mutool -> m_muonMCTrigMatch);	 
  ATH_MSG_INFO("++ Electrons ");
  ATH_MSG_INFO("++ ElecMaxEta        " << eltool -> m_elecMaxEta);		 
  ATH_MSG_INFO("++ ElecMinFwdPt         " << eltool -> m_elecMinFwdPt);		 
  ATH_MSG_INFO("++ ElecMaxFwdEta        " << eltool -> m_elecMaxFwdEta);		 
  ATH_MSG_INFO("++ ElecMinPt         " << eltool -> m_elecMinPt);		 
  ATH_MSG_INFO("++ ElecMinEtaCrack   " << eltool -> m_elecMinEtaCrack);      
  ATH_MSG_INFO("++ ElecMaxEtaCrack   " << eltool -> m_elecMaxEtaCrack);      
  ATH_MSG_INFO("++ ElecFinalMinPt    " << eltool -> m_elecFinalMinPt);	     
  ATH_MSG_INFO("++ ElecTrigMatch     " << m_elecTrigMatch);  	 
  ATH_MSG_INFO("++ ElecMCTrigMatch   " << m_elecMCTrigMatch);	 
  ATH_MSG_INFO("++ ElecPtMaxTrig     " << eltool -> m_elecPtMaxTrig); 	 
  ATH_MSG_INFO("+++++++++++++++++++++++++++++++++++++++++++++++++ ");
  ATH_MSG_INFO("++++ GRL and PU initialisation ");
  ATH_MSG_INFO("++ LumiCalcFile			" << m_LumiCalcFile);	  
  ATH_MSG_INFO("++ PUConfFile 			" << m_PUConfFile);	
  ATH_MSG_INFO("++ PUUnrepresentedDataAction 	" << m_PUUnrepresentedDataAction);
  ATH_MSG_INFO("++ PUDataScaleFactor 		" << m_PUDataScaleFactor); 
  ATH_MSG_INFO("++ PUDefaultChannel		" << m_PUDefaultChannel); 
  ATH_MSG_INFO("++ GRLlist			" << m_GRLlist);	 
  ATH_MSG_INFO("++ GRLPassThrough		" << m_GRLPassThrough);    
  ATH_MSG_INFO("+++++++++++++++++++++++++++++++++++++++++++++++++ ");
  ATH_MSG_INFO("++++ Jet tools initialisation ");
  ATH_MSG_INFO("++ JetAlgo			" << jettool -> m_JetAlgo);	   
  ATH_MSG_INFO("++ JetConfig			" << jettool -> m_JetConfig);	   
  ATH_MSG_INFO("++ JetCalibSeq			" << jettool -> m_JetCalibSeq);	 
  ATH_MSG_INFO("++ JetCleaningCutLevel		" << jettool -> m_JetCleaningCutLevel);
  ATH_MSG_INFO("++ JetCleaningDoUgly		" << jettool -> m_JetCleaningDoUgly); 
  ATH_MSG_INFO("++ JERPlotFileName		" << jettool -> m_JERPlotFileName);   
  ATH_MSG_INFO("++ JERCollectionName		" << jettool -> m_JERCollectionName);
  ATH_MSG_INFO("++ JetSmearingApplyNominal	" << jettool -> m_JetSmearingApplyNominal);
  ATH_MSG_INFO("++ JetSmearingSystematicMode	" << jettool -> m_JetSmearingSystematicMode);
  ATH_MSG_INFO("++ JesUncerJetDefinition	" << jettool -> m_JesUncerJetDefinition);
  ATH_MSG_INFO("++ JesUncerMCType		" << jettool -> m_JesUncerMCType);   
  ATH_MSG_INFO("++ JesUncerConfigFile		" << jettool -> m_JesUncerConfigFile);
  ATH_MSG_INFO("+++++++++++++++++++++++++++++++++++++++++++++++++ ");
  ATH_MSG_INFO("++++ Muon tools initialisation ");
  ATH_MSG_INFO("++ MuToolTTVAWP			" << mutool -> m_MuToolTTVAWP);	   
  ATH_MSG_INFO("++ MuToolTTVACalib		" << mutool -> m_MuToolTTVACalib);     
  ATH_MSG_INFO("++ MuToolEffiWP			" << mutool -> m_MuToolEffiWP);	   
  ATH_MSG_INFO("++ MuToolEffiCalib		" << mutool -> m_MuToolEffiCalib);     
  ATH_MSG_INFO("++ MuToolIsoWP			" << mutool -> m_MuToolIsoWP);	     
  ATH_MSG_INFO("++ MuToolCalibAndSmearStatComb		" << mutool -> m_MuToolCalibAndSmearStatComb);	    
  ATH_MSG_INFO("++ MuToolTrigEffMuQual		" << mutool -> m_MuToolTrigEffMuQual);	    
  ATH_MSG_INFO("++ MuToolCalibAndSmearYear	" << mutool -> m_MuToolCalibAndSmearYear);
  ATH_MSG_INFO("++ MuToolCalibAndSmearAlgo	" << mutool -> m_MuToolCalibAndSmearAlgo);
  ATH_MSG_INFO("++ MuToolCalibAndSmearType	" << mutool -> m_MuToolCalibAndSmearType);
  ATH_MSG_INFO("++ MuToolCalibAndSmearRel	" << mutool -> m_MuToolCalibAndSmearRel); 
  ATH_MSG_INFO("+++++++++++++++++++++++++++++++++++++++++++++++++ ");
  ATH_MSG_INFO("++++ Elec tools initialisation ");
  ATH_MSG_INFO("++ ElToolRecoCorrFile			" << m_ElToolRecoCorrFile);	 
  ATH_MSG_INFO("++ ElToolLLHCorrFile			" << m_ElToolLLHCorrFile);	   
  ATH_MSG_INFO("++ ElToolIsoCorrFile			" << m_ElToolIsoCorrFile);	  
  ATH_MSG_INFO("++ ElToolTrigCorrFile			" << m_ElToolTrigCorrFile);	  
  ATH_MSG_INFO("++ ElToolTrigEffCorrFile		" << m_ElToolTrigEffCorrFile);	 
  ATH_MSG_INFO("++ ElToolForcedataType			" << eltool -> m_ElToolForcedataType);	 
  ATH_MSG_INFO("++ ElToolCalibAndSmearESModel		" << eltool -> m_ElToolCalibAndSmearESModel);
  ATH_MSG_INFO("++ ElToolCalibAndSmearDecorrModel	" << eltool -> m_ElToolCalibAndSmearDecorrModel);
  ATH_MSG_INFO("++ ElToolLlhPVCont			" << eltool -> m_ElToolLlhPVCont);	  
  ATH_MSG_INFO("++ ElToolLlhLooseConfigFile			" << eltool -> m_ElToolLlhLooseConfigFile);
  ATH_MSG_INFO("++ ElToolLlhMediumConfigFile			" << eltool -> m_ElToolLlhMediumConfigFile);
  ATH_MSG_INFO("++ ElToolLlhTightConfigFile			" << eltool -> m_ElToolLlhTightConfigFile);	  
  ATH_MSG_INFO("++ ElIDToolFwdConfigFile			" << eltool -> m_ElFwdIDConfigFile);	  
  ATH_MSG_INFO("+++++++++++++++++++++++++++++++++++++++++++++++++ ");
  ATH_MSG_INFO("++++ Isolation tool for both el and mu initialisation ");
  ATH_MSG_INFO("++ IsolToolMuonWP	" << mutool -> m_IsolToolMuonWP);
  ATH_MSG_INFO("++ IsolToolElecWP	" << eltool -> m_IsolToolElecWP);
  ATH_MSG_INFO("+++++++++++++++++++++++++++++++++++++++++++++++++ ");
  ATH_MSG_INFO("++++ Trigger decision tool initialisation ");
  ATH_MSG_INFO("++ TrigDecisionKey	" << trigtool -> m_TrigDecisionKey);  
  ATH_MSG_INFO("++ passTriggerElecData	" << m_passTriggerElecData);
  ATH_MSG_INFO("++ passTriggerElecMC	" << m_passTriggerElecMC); 
  ATH_MSG_INFO("++ passTriggerMuonData	" << m_passTriggerMuonData);
  ATH_MSG_INFO("++ passTriggerMuonMC	" << m_passTriggerMuonMC); 
  ATH_MSG_INFO("+++++++++++++++++++++++++++++++++++++++++++++++++ ");
  ATH_MSG_INFO("++++ MET tool initialisation ");
  ATH_MSG_INFO("++ METJetSelection	" << mettool -> m_METJetSelection);
  ATH_MSG_INFO("+++++++++++++++++++++++++++++++++++++++++++++++++ ");
  ATH_MSG_INFO("++++ End of dump config file ");
  ATH_MSG_INFO("+++++++++++++++++++++++++++++++++++++++++++++++++ \n ");
  ATH_MSG_INFO(" \n ");
  return;
}


// Initialise the tools
void initfinal::InitialisePRW() {
  //If we set m_pureweighting to 0 and don't do the following lines, there is a crash in muon tools initialisation : it is necessary to have a pile up reweighting tool set up
  //if(m_pureweighting){
  m_PileupReweighting = new CP::PileupReweightingTool("PileupReweighting");
  // create config file vectors
  std::vector<std::string> confFiles;
  std::vector<std::string> lcalcFiles;
  for(TString str : m_vecLumiCalcFile)
    lcalcFiles.push_back(m_rootCoreBin + str.Data());
  for(TString str : m_vecPUConfFile)
    confFiles.push_back(m_rootCoreBin + str.Data());

  //If you switch the configuration off and enable WriteToFile() in "finalize" you can produce new config files
  m_PileupReweighting->setProperty( "ConfigFiles", confFiles);
  m_PileupReweighting->setProperty( "LumiCalcFiles", lcalcFiles); 

  m_PileupReweighting->setProperty("UnrepresentedDataAction",m_PUUnrepresentedDataAction);

  //m_PileupReweighting->setProperty("IgnoreBadChannels",false);
  //m_PileupReweighting->setProperty("EnableDebugging",true);
  m_PileupReweighting->setProperty("DataScaleFactor",m_PUDataScaleFactor);
  //m_PileupReweighting->setProperty("DefaultChannel",m_PUDefaultChannel);
  if (!m_PileupReweighting->initialize().isSuccess()) { // check this isSuccess  
    Error("initialize()", "Failed to properly initialize the Pileup. Exiting." );
    exit(10);
  }
}

// Initialise the GRL tool
void initfinal::InitialiseGRL() {
  m_grlTool = new GoodRunsListSelectionTool("GoodRunsListSelectionTool");
  m_grlTool->setProperty( "OutputLevel", MSG::INFO); 
  std::vector<std::string> vecStringGRL;
  for(TString str : m_GRLVec) vecStringGRL.push_back(m_rootCoreBin + str.Data());
  m_grlTool->setProperty( "GoodRunsListVec", vecStringGRL);
  m_grlTool->setProperty("PassThrough", m_GRLPassThrough);
  if (!m_grlTool->initialize().isSuccess()) 
  {
    std::cout << "Failed to properly initialize the GRL!" << std::endl; 
    abort();
  }   
}

void initfinal::InitSystVariation(){
  // loop over systematics registry:
  //const CP::SystematicRegistry& registry = CP::SystematicRegistry::getInstance();
  //const CP::SystematicSet& recommendedSystematics = registry.recommendedSystematics();

  m_sysListCB.push_back(CP::SystematicSet());

  if( !m_dosys)
    return;
  m_sysListSF_corr = new std::map<std::string, std::vector<CP::SystematicSet> *>();	
  m_sysListSF_uncorr = new std::map<std::string, std::map<std::string, CP::SystematicSet *> *>();	
  if(m_dosysEL){
    InsertSystFULL(eltool->m_elLLHCorr_medium, &_listSF1, &_listSF2);
    //InsertSystFULL(eltool->m_elLLHCorr_loose, &_listSF1, &_listSF2);

    InsertSystFULL(eltool->m_elIsoCorr_medium, &_listSF3, &_listSF4);
    //probably completely useless
    //InsertSystFULL(eltool->m_elLLHCorr_tight, &_listSF1, &_listSF2);
    //InsertSystFULL(eltool->m_elIsoCorr_tight, &_listSF3, &_listSF4);
    InsertSystFULL(eltool->m_elRecoCorr, &_listSF5, &_listSF6);
    InsertSystFULL(eltool->m_elTrigCorr, &_listSF7, &_listSF8);
    //will work on this later
    InsertSyst(eltool->m_egammaCalibrationAndSmearingTool, &m_sysListCB);
    MapSyst(&_listSF2, &_listuncorr1);	
    MapSyst(&_listSF4, &_listuncorr2);	
    MapSyst(&_listSF6, &_listuncorr3);	
    MapSyst(&_listSF8, &_listuncorr4);	
    //odd is fully correlated systematic, even is the fully uncorrelated part (statistical)
    (*m_sysListSF_corr)["m_elLLHCorr"] = &_listSF1;
    (*m_sysListSF_uncorr)["m_elLLHCorr"] = &_listuncorr1;
    (*m_sysListSF_corr)["m_elIsoCorr"] = &_listSF3;
    (*m_sysListSF_uncorr)["m_elIsoCorr"] = &_listuncorr2;
    (*m_sysListSF_corr)["m_elRecoCorr"] = &_listSF5;
    (*m_sysListSF_uncorr)["m_elRecoCorr"] = &_listuncorr3;
    (*m_sysListSF_corr)["m_elTrigCorr"] = &_listSF7;
    (*m_sysListSF_uncorr)["m_elTrigCorr"] = &_listuncorr4;	
    }
    
  if(m_dosysMU){
    InsertSystFULL(mutool->m_effi_medium_corr,&_listSF9,&_listSF10, sysname_muSF_effi);
    //presumably not needed
    //InsertSystFULL(mutool->m_effi_tight_corr,&_listSF9t);
    InsertSystFULL(mutool->m_iso_corr, &_listSF11, &_listSF12, sysname_muSF_iso);
    InsertSystFULL(mutool->m_TTVA_effi_corr, &_listSF13, &_listSF14, sysname_muSF_ttva);
    InsertSyst(mutool->m_muTrigEff, &_listSF15);
    //	InsertSystMCTOY(m_elTrigEff, &_listSF9); 
    InsertSyst(mutool->m_muonCalibrationAndSmearingTool, &m_sysListCB);
    MapSyst(&_listSF10, &_listuncorr5);	
    MapSyst(&_listSF12, &_listuncorr6);	
    MapSyst(&_listSF14, &_listuncorr7);	
    //trigger SF :for later
    //MapSyst(&_listSF16, &_listuncorr8);	
    
    (*m_sysListSF_corr)["m_effi_corr"] = &_listSF9;
    (*m_sysListSF_uncorr)["m_effi_corr"] = &_listuncorr5;
    //this is presumably the same systematic list
    //(*m_sysListSF_corr)["m_effi_tight_corr"] = &_listSF9t;
    (*m_sysListSF_corr)["m_iso_corr"] = &_listSF11;
    (*m_sysListSF_uncorr)["m_iso_corr"] = &_listuncorr6;
    (*m_sysListSF_corr)["m_TTVA_effi_corr"] = &_listSF13;
    (*m_sysListSF_uncorr)["m_TTVA_effi_corr"] = &_listuncorr7;	
    (*m_sysListSF_corr)["m_muTrigEff"] = &_listSF15;	
    //trigger SF TBD later
    //(*m_sysListSF_uncorr)["m_muTrigEff"] = &_listuncorr8;
  }

  sysnames.push_back("nominal");
    

  std::vector<CP::SystematicSet>::const_iterator sysItr;
  for(sysItr = m_sysListCB.begin(); sysItr != m_sysListCB.end(); ++sysItr){
    std::string sysname = (*sysItr).name();
    if((*sysItr).name()=="") sysname = "nominal";
    //excluding photon energy scale sytematic
    std::string substr="PH_";
    if(std::strncmp(sysname.c_str(), substr.c_str(), substr.size()) == 0)
      continue;
    std::cout << "Adding calib systematic : "<<sysname <<std::endl;
    sysnames.push_back(sysname);
  } // end for loop over calibration systematics


  for(auto sysSFItr = m_sysListSF_corr->begin(); sysSFItr != m_sysListSF_corr->end(); ++sysSFItr){
    for( sysItr = (sysSFItr->second)->begin(); sysItr != (sysSFItr->second)->end(); ++sysItr){
      std::string sysname = (*sysItr).name();
      std::cout << "Adding SF systematic (corr) : "<<sysname <<std::endl;
      sysnames.push_back(sysname);
    } // end for loop over correlated SF uncertainties (SYST)
  }


  for(auto sysSFItr = m_sysListSF_uncorr->begin(); sysSFItr != m_sysListSF_uncorr->end(); ++sysSFItr){
    for( auto sysMapItr = (sysSFItr->second)->begin(); sysMapItr != (sysSFItr->second)->end(); ++sysMapItr){
      //excluding photon energy scale sytematic
      std::string sysname = (sysMapItr->second)->name();
      std::cout << "Adding systematic (uncorr) : "<<sysname <<std::endl;
      sysnames.push_back(sysname);
    } // end for loop over unorrelated SF uncertainties (STAT)
  }

}
// Initialise

void initfinal::MapSyst(std::vector<CP::SystematicSet > * systList, std::map<std::string, CP::SystematicSet*> * systMap ){
  for(unsigned int i =0; i<systList->size(); i++ ){
    (*systMap)[systList->at(i).name()] = &(systList->at(i));
  }
}

void initfinal::InsertSystFULL(AsgElectronEfficiencyCorrectionTool * rcTool, std::vector<CP::SystematicSet> * systList_corr, std::vector<CP::SystematicSet > * systList_uncorr){
  CP::SystematicSet recommendedSystematics = rcTool->recommendedSystematics();
  std::vector<CP::SystematicSet> rcSysts;
  CP::MakeSystematicsVector sysVec;
  sysVec.calc(recommendedSystematics);
  rcSysts=sysVec.result("");
  rcSysts.erase(rcSysts.begin());
  for(auto sys = rcSysts.begin(); sys!= rcSysts.end(); sys++){
    TString sysname = sys->name();
    if(sysname.Contains("Corr")) systList_corr->push_back(*sys);
    else systList_uncorr->push_back(*sys);
  }			
}

void initfinal::InsertSystFULL(CP::MuonEfficiencyScaleFactors * rcTool, std::vector<CP::SystematicSet> * systList_corr , std::vector<CP::SystematicSet> * systList_uncorr, std::map<std::string, std::string> &maptool ){
  CP::SystematicSet recommendedSystematics = rcTool->recommendedSystematics();
  std::vector<CP::SystematicSet> rcSysts;
  CP::MakeSystematicsVector sysVec;
  sysVec.calc(recommendedSystematics);
  rcSysts=sysVec.result("");
  rcSysts.erase(rcSysts.begin());
  for(auto sys = rcSysts.begin(); sys!= rcSysts.end(); sys++){
    //std::cout << "syst size = " <<  sys->size() << std::endl;
    if((sys->begin())->isToyVariation() ){
      systList_uncorr->push_back(*sys);
      std::string sysname = sys->name();
      std::string binname = rcTool->getUncorrelatedSysBinName(*sys);
      std::string s = sysname.substr(sysname.find("_STAT_"),sysname.size()-sysname.find("_STAT_")-10 );
      maptool[binname]=s;
      }
    /*
    std::string sysname = (*sysItr).name();
    if(sysname.Contains("Corr")) systList_corr->push_back(*sys);*/
    else
      systList_corr->push_back(*sys);
  }
}
void initfinal::InsertSyst(AsgElectronEfficiencyCorrectionTool * rcTool, std::vector<CP::SystematicSet> * systList ){
  CP::SystematicSet recommendedSystematics = rcTool->recommendedSystematics();
  std::vector<CP::SystematicSet> rcSysts;
  CP::MakeSystematicsVector sysVec;
  sysVec.calc(recommendedSystematics);
  rcSysts=sysVec.result("");
  rcSysts.erase(rcSysts.begin());
  systList->insert(systList->end(),rcSysts.begin(), rcSysts.end());
}
void initfinal::InsertSyst(CP::EgammaCalibrationAndSmearingTool * rcTool, std::vector<CP::SystematicSet> * systList ){
  CP::SystematicSet recommendedSystematics = rcTool->recommendedSystematics();
  std::vector<CP::SystematicSet> rcSysts;
  CP::MakeSystematicsVector sysVec;
  sysVec.calc(recommendedSystematics);
  rcSysts=sysVec.result("");
  rcSysts.erase(rcSysts.begin());
  for(auto sys = rcSysts.begin(); sys!= rcSysts.end(); sys++){
    TString sysname = sys->name();//excluding photon energy scale sytematic
    if(!(sysname.BeginsWith("PH_"))) systList->push_back(*sys);
  }	
}
void initfinal::InsertSyst(CP::MuonCalibrationAndSmearingTool * rcTool, std::vector<CP::SystematicSet> * systList ){
  CP::SystematicSet recommendedSystematics = rcTool->recommendedSystematics();
  std::vector<CP::SystematicSet> rcSysts;
  CP::MakeSystematicsVector sysVec;
  sysVec.calc(recommendedSystematics);
  rcSysts=sysVec.result("");
  rcSysts.erase(rcSysts.begin());
  systList->insert(systList->end(),rcSysts.begin(), rcSysts.end());
}

void initfinal::InsertSyst(CP::MuonEfficiencyScaleFactors * rcTool, std::vector<CP::SystematicSet> * systList ){
  CP::SystematicSet recommendedSystematics = rcTool->recommendedSystematics();
  std::vector<CP::SystematicSet> rcSysts;
  CP::MakeSystematicsVector sysVec;
  sysVec.calc(recommendedSystematics);
  rcSysts=sysVec.result("");
  rcSysts.erase(rcSysts.begin());
  systList->insert(systList->end(),rcSysts.begin(), rcSysts.end());
}

void initfinal::InsertSyst(CP::MuonTriggerScaleFactors * rcTool, std::vector<CP::SystematicSet> * systList ){
  CP::SystematicSet recommendedSystematics = rcTool->recommendedSystematics();
  std::vector<CP::SystematicSet> rcSysts;
  CP::MakeSystematicsVector sysVec;
  sysVec.calc(recommendedSystematics);
  rcSysts=sysVec.result("");
  rcSysts.erase(rcSysts.begin());
  systList->insert(systList->end(),rcSysts.begin(), rcSysts.end());
}


//Finalise
//-systematic loop
//-event loop
//-the whole program

void initfinal::FinaliseSyst() {
  ana->clearCont();
  ana->mini->clearVec();
  mctruth->finalise();
}

// Finalise
void initfinal::Finalise() {

  ana->WriteTree(isPowheg);

  // delete tools
  std::cout << "Deleting tools..." << std::endl;
  if( m_grlTool ) {
    delete m_grlTool;
    m_grlTool = 0;
  }
  if( m_pureweighting ) {
    delete m_PileupReweighting;
    m_PileupReweighting = 0;
  }
  if( jettool->m_jetCalibTool ) {
    delete jettool->m_jetCalibTool;
    jettool->m_jetCalibTool = 0;
  }
  if( jettool->m_jetCleaningTool ) {
    delete jettool->m_jetCleaningTool;
    jettool->m_jetCleaningTool = 0;
  }
  /*
     if( jettool->m_jesUncertainty ) {
     delete jettool->m_jesUncertainty;
     jettool->m_jesUncertainty = 0;
     }
     */
  if( jettool->m_jetSmearingTool ) {
    delete jettool->m_jetSmearingTool;
    jettool->m_jetSmearingTool = 0;
  }
  if ( jettool->m_jetJVTTool ) {
    delete jettool->m_jetJVTTool;
    jettool->m_jetJVTTool = 0;
  }
  if( jettool->m_JERTool ) {
    delete jettool->m_JERTool;
    jettool->m_JERTool = 0;
  }
  if(mutool->m_muonCalibrationAndSmearingTool){
    delete mutool->m_muonCalibrationAndSmearingTool;
    mutool->m_muonCalibrationAndSmearingTool = 0;
  }
  if(m_isMC){
    if(mutool->m_TTVA_effi_corr){
      delete mutool->m_TTVA_effi_corr;
      mutool->m_TTVA_effi_corr = 0;
    }
    if(mutool->m_effi_medium_corr){
      delete mutool->m_effi_medium_corr;
      mutool->m_effi_medium_corr = 0;
    }
    if(mutool->m_effi_tight_corr){
      delete mutool->m_effi_tight_corr;
      mutool->m_effi_tight_corr = 0;
    }
    if(mutool->m_iso_corr){
      delete mutool->m_iso_corr;
      mutool->m_iso_corr = 0;
    }
  }
  if(mutool->m_muonSelection){
    delete mutool->m_muonSelection;
    mutool->m_muonSelection = 0;
  }
  //if(m_electronisoTool){
  //	delete m_electronisoTool;
  //	m_electronisoTool = 0;
  //}
  if(eltool->m_elRecoCorr){
    delete eltool->m_elRecoCorr;
    eltool->m_elRecoCorr = 0;
  }
   if(eltool->m_elLLHCorr_loose){
    delete eltool->m_elLLHCorr_loose;
    eltool->m_elLLHCorr_loose = 0;
  }
  if(eltool->m_elLLHCorr_medium){
    delete eltool->m_elLLHCorr_medium;
    eltool->m_elLLHCorr_medium = 0;
  }
  if(eltool->m_elIsoCorr_medium){
    delete eltool->m_elIsoCorr_medium;
    eltool->m_elIsoCorr_medium = 0;
  }
  if(eltool->m_elLLHCorr_tight){
    delete eltool->m_elLLHCorr_tight;
    eltool->m_elLLHCorr_tight = 0;
  }
  if(eltool->m_elIsoCorr_tight){
    delete eltool->m_elIsoCorr_tight;
    eltool->m_elIsoCorr_tight = 0;
  }
  if(eltool->m_elTrigCorr){
    delete eltool->m_elTrigCorr;
    eltool->m_elTrigCorr = 0;
  }
  if(eltool->m_egammaCalibrationAndSmearingTool){
    delete eltool->m_egammaCalibrationAndSmearingTool;
    eltool->m_egammaCalibrationAndSmearingTool = 0;
  }
  if(eltool->m_electronLikelihoodToolLoose){
    delete eltool->m_electronLikelihoodToolLoose;
    eltool->m_electronLikelihoodToolLoose = 0;
  }
  if(eltool->m_electronLikelihoodToolMedium){
    delete eltool->m_electronLikelihoodToolMedium;
    eltool->m_electronLikelihoodToolMedium = 0;
  }
  if(eltool->m_electronLikelihoodToolTight){
    delete eltool->m_electronLikelihoodToolTight;
    eltool->m_electronLikelihoodToolTight = 0;
  }
  if(eltool->m_electronFwdIDTool){
    delete eltool->m_electronFwdIDTool;
    eltool->m_electronFwdIDTool = 0;
  }
  if(mettool->m_hadrecoil){
    mettool->m_hadrecoil ->finalize();
    delete mettool->m_hadrecoil;
    mettool->m_hadrecoil = 0;
  }
  if(mctruth){
    delete mctruth;
    mctruth = 0;
  }	
  //xAOD::IOStats::instance().stats().printSmartSlimmingBranchList();

}



#endif

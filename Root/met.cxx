#ifndef MET_CXX
#define MET_CXX

#include "MiniTreeMaker13TeV/met.h"

// Constructor
MET::MET(){		
  return;
}
// Destructor
MET::~MET() {
}


int MET::executeRecoil(xAOD::TEvent& m_event, xAOD::IParticleContainer* leptons){

  //std::cout << "getting recoil " << std::endl;
  PFOHR = m_hadrecoil -> getHadRecoilPFO(m_event, leptons);
  //std::cout << "getting ncones " << std::endl;
  u_ncones = m_hadrecoil->ncones;
  //std::cout << "getting EM recoil  " << std::endl;
  PFOHREM = m_hadrecoil -> getHadRecoilPFO(m_event, leptons, true);
  //std::cout << "getting recoil chrged " << std::endl;
  PFOHRCharged = m_hadrecoil -> getHadRecoilPFOCharged(m_event, leptons);
  //std::cout << "getting neutral " << std::endl;
  PFOHRNeutral = m_hadrecoil -> getHadRecoilPFONeutral(m_event, leptons) ;
  //std::cout << "getting recoil neutral EM " << std::endl;
  PFOHRNeutralEM = m_hadrecoil -> getHadRecoilPFONeutral(m_event, leptons, true);
  //TLorentzVector PFOHR;
  //Voronoi not working very well at the moment
  PFOVoronoiHR = m_hadrecoil -> getHadRecoilPFOVoronoi(m_event, leptons);
  //TLorentzVector PFOVoronoiSpreadingHR;
  //PFOVoronoiSpreadingHR = m_hadrecoil -> getHadRecoilPFOVoronoiSpreading(m_event, leptons);
  SET = m_hadrecoil -> getSumET_PFO(m_event, leptons);
  //nSET = m_hadrecoil -> getSumET_nPFO(m_event, leptons);
  nSET = 0.;

  return 0;
}


int MET::getMET(xAOD::TEvent& m_event, xAOD::MissingETContainer *m_MetContainer, xAOD::ElectronContainer *goodElecs, xAOD::MuonContainer *goodMuons, xAOD::JetContainer *goodJets, float m_elecFinalMinPt, float m_muonFinalMinPt){

  static std::string jetType = "AntiKt4EMTopoJets";
  const xAOD::MissingETContainer* coreMet  = nullptr;
  std::string coreMetKey = "MET_Core_" + jetType;
  coreMetKey.erase(coreMetKey.length() - 4);//this removes the Jets from the end of the jetType
  assert( m_event.retrieve(coreMet, coreMetKey) );
  //m_event.record(coreMet, "CoreMET");
  //retrieve the MET association map
  const xAOD::MissingETAssociationMap* metMap = nullptr;
  std::string metAssocKey = "METAssoc_" + jetType;
  metAssocKey.erase(metAssocKey.length() - 4 );//this removes the Jets from the end of the jetType
  assert( m_event.retrieve(metMap, metAssocKey) );

  metMap->resetObjSelectionFlags();
  ConstDataVector<xAOD::ElectronContainer> metElectrons(SG::VIEW_ELEMENTS);
  for(const auto& el : *goodElecs) {
    //only use electrons of the final selection
    if(el->pt() < m_elecFinalMinPt)continue;
    metElectrons.push_back(el);
  }
  ConstDataVector<xAOD::MuonContainer> metMuons(SG::VIEW_ELEMENTS);
  for(const auto& mu : *goodMuons) {
    //only use muons of the final selection
    if(mu->pt() < m_muonFinalMinPt)continue;
    metMuons.push_back(mu);
  }
  m_METMaker->rebuildMET("RefEle", xAOD::Type::Electron, m_MetContainer, metElectrons.asDataVector(), metMap);
  m_METMaker->rebuildMET("Muons", xAOD::Type::Muon, m_MetContainer, metMuons.asDataVector(), metMap);
  //Jet selection
  ConstDataVector<xAOD::JetContainer> selectedJets(SG::VIEW_ELEMENTS);
  for(const auto& jet : *goodJets) {
    //additional jet selection : if needed add cuts
    selectedJets.push_back(jet);
  }
  m_METMaker->rebuildJetMET("RefJet", "SoftClus", "PVSoftTrk", m_MetContainer, selectedJets.asDataVector(), coreMet, metMap, true);  

  //assert( m_METMaker->buildMETSum("FinalClus", m_newMetContainer, MissingETBase::Source::LCTopo) );
  assert( m_METMaker->buildMETSum("FinalTrk" , m_MetContainer, MissingETBase::Source::Track ) );

  return 0;


}




void MET::InitialiseMETTools(){
  m_METMaker.setTypeAndName("met::METMaker/metMaker");
  assert(m_METMaker.setProperty( "JetSelection" , m_METJetSelection.Data()) );
  //assert( m_METMaker.setProperty("DoMuonEloss", true) );
  //assert( m_METMaker.setProperty("DoIsolMuonEloss", true) );
  assert( m_METMaker.retrieve() );  
  /*
     m_METMaker = new MET::METMaker("m_METMaker");
     ToolHandle<IMETMaker> metHandle(m_METMaker->name());
     m_METMaker->setProperty("ConfigTool", metHandle);
     m_METMaker->setProperty( "JetSelection" , m_METJetSelection.Data()) ; // Tight is for W/Z inclusive selections (can switch to Default if needed)
     m_METMaker->initialize();*/
  //alternative MET (PFOs, Voronoi...)
  m_hadrecoil = new hadrecoil("m_hadrecoilTool", 0.2, 0.4, m_cutPFOneutral);
  m_hadrecoil ->initialize();

}


#endif

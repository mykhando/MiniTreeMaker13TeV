#ifndef MUONS_CXX
#define MUONS_CXX

#include "MiniTreeMaker13TeV/muons.h"

// Constructor
muons::muons() {		
  return;
}
// Destructor
muons::~muons() {
}

int muons::StandardMuonSelection(xAOD::TEvent& m_event,  const xAOD::EventInfo& eventInfo, xAOD::MuonContainer* mucont,  xAOD::MuonContainer* inclMuons, asg::AnaToolHandle<Trig::IMatchingTool> m_muTrigMatch){
  int NPV = 0; int NPUV = 0;
  const xAOD::VertexContainer* vertices = NULL;
  m_event.retrieve(vertices,"PrimaryVertices");
  xAOD::VertexContainer::const_iterator vitr;
  const xAOD::Vertex *pv(0);
  for (vitr = vertices->begin(); vitr != vertices->end(); ++vitr){
    if ( (*vitr)->vertexType() == xAOD::VxType::PriVtx) {
      NPV++;
      if(NPV==1) pv = (*vitr); 
    }
    if ( (*vitr)->vertexType() == xAOD::VxType::PileUp) NPUV++;   
  }
  if(!pv){
    Error("muons::StandardMuonSelection", "Failed to retrieve Primary Vertex. Exiting." );
    return -1;
  }
  const xAOD::MuonContainer* muons = 0;
  if ( !m_event.retrieve( muons, "Muons" ).isSuccess() )
  { // retrieve arguments: container type, container key
    Error("execute()", "Failed to retrieve Muons container. Exiting." );
    return -1;
  }

  // create a shallow copy of the muons container
  std::pair< xAOD::MuonContainer*, xAOD::ShallowAuxContainer* > muons_shallowCopy = xAOD::shallowCopyContainer( *muons );
  if(!xAOD::setOriginalObjectLink(*muons, *(muons_shallowCopy.first)))
  {//tell calib container what old container it matches
    std::cout << "Failed to set the original object links" << std::endl;
    return -1;
  }

  // iterate over our shallow copy
  xAOD::MuonContainer::iterator muonSC_itr = (muons_shallowCopy.first)->begin();
  xAOD::MuonContainer::iterator muonSC_end = (muons_shallowCopy.first)->end();
  ngoodmu =0;
  for( ; muonSC_itr != muonSC_end; ++muonSC_itr ) {
    (*muonSC_itr)->auxdecor< bool >( "inrecoil") = false;
    if(m_muonCalibrationAndSmearingTool->applyCorrection(**muonSC_itr) == CP::CorrectionCode::Error)
    {// apply correction and check return code
      // Can have CorrectionCode values of Ok, OutOfValidityRange, or Error. Here only checking for Error.
      // If OutOfValidityRange is returned no modification is made and the original muon values are taken.
      Error("execute()", "MuonCalibrationAndSmearingTool returns Error CorrectionCode");
    }

    bool    mu_iso = m_isolationSelectionTool-> accept(**muonSC_itr);
    bool    mu_acc = m_muonSelection->accept(**muonSC_itr);
    bool    mu_acc_incl = m_muonInclSelection->accept(**muonSC_itr);

    const xAOD::TrackParticle *mutrk = (*muonSC_itr)->primaryTrackParticle();
    //if no track particle, skip event
    if(!mutrk) continue;
    // require : medium muon, pT > 5 Gev
    if(!mu_acc_incl || ((*muonSC_itr)->pt()<m_muonMinPtIncl))  continue;
    float mu_d0sig = 10.,mu_deltaz0=10.,mu_deltaz0new=10.;
    if(pv && mutrk){
      mu_d0sig = xAOD::TrackingHelpers::d0significance( mutrk, eventInfo.beamPosSigmaX(), eventInfo.beamPosSigmaY(), eventInfo.beamPosSigmaXY() );
      mu_deltaz0 = fabs(mutrk->z0() + mutrk->vz() - pv->z());
      mu_deltaz0new = mu_deltaz0 * sin(mutrk->theta());
      (*muonSC_itr)-> auxdata<float>( "deltaz0new" ) = mu_deltaz0new;
      (*muonSC_itr)-> auxdata<float>( "d0sig" ) = mu_d0sig;
    }
    bool isMuVtxAssoc = (fabs(mu_d0sig)<3) && (fabs(mu_deltaz0new)<0.5); 
    xAOD::Muon* inclMu = new xAOD::Muon();
    inclMu->makePrivateStore((**muonSC_itr));
    inclMuons->push_back(inclMu); 	
    //require : pT >18 GeV		
    if((*muonSC_itr)->pt()<m_muonMinPt) continue;
    //	if(! (fabs((*muonSC_itr)->eta())<m_muonMaxEta)) continue; 
    //check if it's tight
    (*muonSC_itr)-> auxdata<bool>( "isTight" ) = mu_acc;
    (*muonSC_itr)-> auxdata<bool>( "isIsolated" ) = mu_iso;
    //require : vertex association -- relax for the moment
    //if(!isMuVtxAssoc)   continue;
    //require : isolation -- relax for the moment
    //if(!mu_iso ) continue;
    //count number of 'final selection' muons :if == 0 we do not consider this as muon channel event -- actually relaxed as well
    if((*muonSC_itr)->pt() > m_muonFinalMinPt && mu_acc) ngoodmu++;

    xAOD::Muon* tempMu = new xAOD::Muon();
    tempMu ->makePrivateStore((**muonSC_itr));
    bool  muTrMatch = 0;
    std::vector<const xAOD::IParticle*> myMuons(1,*muonSC_itr);
    //note : does not separete data and MC trigger chains like elecs does for the moment. Is this really useful ?
    for(TString str : muontrigmatchnames){
      //if(m_muTrigMatch->match( *muonSC_itr, str.Data() )) muTrMatch = 1;
      //for non stdm4
      if(m_muTrigMatch->match( myMuons, str.Data() )) muTrMatch = 1;
      //for stdm4 : will be back later ???
      //TString trigname="STDM4_"+str;
      //if(tempMu->auxdata< char >( trigname.Data() ))muTrMatch = 1;
    }
    //done during gettrigger -- but works for STDM4, have to make a switch
    tempMu->auxdata< bool >( "trigMatched" ) = muTrMatch;
    // tempMu->auxdata< float >( "ptvarcone20" ) = i
    float sfiso = 1., sfreco_medium = 1. , sfreco_tight = 1., sfTTVA = 1., effiso = 1.;
    if(m_isMC){
      m_effi_medium_corr->getEfficiencyScaleFactor(**muonSC_itr, sfreco_medium);
      m_effi_tight_corr->getEfficiencyScaleFactor(**muonSC_itr, sfreco_tight);
      //std::cout << " print 1 " << sfreco << " " << sfiso << " " << sfTTVA << std::endl;
      m_iso_corr->getEfficiencyScaleFactor(**muonSC_itr, sfiso);
      //std::cout << " print 2 " << sfreco << " " << sfiso << " " << sfTTVA << std::endl;
      m_iso_corr->getDataEfficiency(**muonSC_itr, effiso);

      m_TTVA_effi_corr->getEfficiencyScaleFactor(**muonSC_itr, sfTTVA);
      //std::cout << " print 3 " << sfreco << " " << sfiso << " " << sfTTVA << std::endl;

      tempMu->auxdata< float >( "sfiso") = sfiso;
      tempMu->auxdata< float >( "sfTTVA") = sfTTVA;
      tempMu->auxdata< float >( "sfreco_medium") = sfreco_medium;
      tempMu->auxdata< float >( "sfreco_tight") = sfreco_tight;
      tempMu->auxdata< std::string >( "rg_sfreco_tight") = m_effi_tight_corr->getUncorrelatedSysBinName(m_effi_tight_corr->getUnCorrelatedSystBin(**muonSC_itr));
      tempMu->auxdata< std::string >( "rg_sfreco_medium") = m_effi_medium_corr->getUncorrelatedSysBinName(m_effi_medium_corr->getUnCorrelatedSystBin(**muonSC_itr));
      tempMu->auxdata< std::string >( "rg_sfTTVA") = m_TTVA_effi_corr->getUncorrelatedSysBinName(m_TTVA_effi_corr->getUnCorrelatedSystBin(**muonSC_itr));
      tempMu->auxdata< std::string >( "rg_sfiso") = m_iso_corr->getUncorrelatedSysBinName(m_iso_corr->getUnCorrelatedSystBin(**muonSC_itr));
    }
    tempMu->auxdata< float >( "deltaz0new" ) = (*muonSC_itr)-> auxdata<float>( "deltaz0new" );
    tempMu->auxdata< float >( "d0sig" ) = (*muonSC_itr)-> auxdata<float>( "d0sig" );
    tempMu->auxdata< float >( "ptvarcone30" ) = (*muonSC_itr)-> auxdata<float>( "ptvarcone30" );
    tempMu->auxdata< float >( "topoetcone20" ) = (*muonSC_itr)-> auxdata<float>( "topoetcone20" );
    mucont->push_back(tempMu);
    //Info("execute()", "  corrected muon pt = %.2f GeV", ((*muonSC_itr)->pt() * 0.001));  

  } // end for loop over shallow copied muons
  delete muons_shallowCopy.first;
  delete muons_shallowCopy.second;

  Double_t temp_sftrig = 1., temp_efftrig = 1.;
  //Compute the muon trigger SF according to the selection :
  //Z selection (2 muons)
  //W selection (use the muon that matched the trigger)
  if(m_isMC){
    ConstDataVector<xAOD::MuonContainer> tempMuonContainer(SG::VIEW_ELEMENTS);
    std::string trigChain = m_muonMCTrigMatch.Data();
    for(const auto& mu : *mucont){
      temp_sftrig = 1.;
      temp_efftrig = 1.;
      tempMuonContainer.push_back(mu);
      m_muTrigEff->getTriggerScaleFactor(*(tempMuonContainer.asDataVector()), temp_sftrig, trigChain );
      m_muTrigEff->getTriggerEfficiency(*mu, temp_efftrig, trigChain, false);
      tempMuonContainer.clear();
      mu->auxdata<float>("sftrig") = temp_sftrig;
      mu->auxdata<float>("trigeff") = temp_efftrig;
    }
  }

  //m_event.record( mucont, "mucont"  );m_event.record( inclMuons, "inclMuons"  );
  //maybe have to keep that (?)
  /*
     m_event.record( mucont, "mucont"  );
     m_event.record( mucontAux, "mucontAux."  );
     m_event.record( inclMuons, "inclMuons"  );
     m_event.record( inclMuonsAux, "inclMuonsAux."  );*/

  if( inclMuons->size() <1 )
    return 1;
  //if( ngoodmu ==0 )
  if(mucont->size() < 1 )
    return 1;
  //std::sort(mucont->begin(),mucont->end(),DescendingPt());
  return 0;

}

int muons::AntiIsoMuonSelection(xAOD::TEvent& m_event,  const xAOD::EventInfo& eventInfo, xAOD::MuonContainer* mucont,  xAOD::MuonContainer* inclMuons, asg::AnaToolHandle<Trig::IMatchingTool> m_muTrigMatch){
  int NPV = 0; int NPUV = 0;
  const xAOD::VertexContainer* vertices = NULL;
  m_event.retrieve(vertices,"PrimaryVertices");
  xAOD::VertexContainer::const_iterator vitr;
  const xAOD::Vertex *pv(0);
  for (vitr = vertices->begin(); vitr != vertices->end(); ++vitr){
    if ( (*vitr)->vertexType() == xAOD::VxType::PriVtx) {
      NPV++;
      if(NPV==1) pv = (*vitr); 
    }
    if ( (*vitr)->vertexType() == xAOD::VxType::PileUp) NPUV++;   
  }
  if(!pv){
    Error("muons::StandardMuonSelection", "Failed to retrieve Primary Vertex. Exiting." );
    return -1;
  }
  const xAOD::MuonContainer* muons = 0;
  if ( !m_event.retrieve( muons, "Muons" ).isSuccess() )
  { // retrieve arguments: container type, container key
    Error("execute()", "Failed to retrieve Muons container. Exiting." );
    return -1;
  }

  // create a shallow copy of the muons container
  std::pair< xAOD::MuonContainer*, xAOD::ShallowAuxContainer* > muons_shallowCopy = xAOD::shallowCopyContainer( *muons );
  if(!xAOD::setOriginalObjectLink(*muons, *(muons_shallowCopy.first)))
  {//tell calib container what old container it matches
    std::cout << "Failed to set the original object links" << std::endl;
    return -1;
  }

  // iterate over our shallow copy
  xAOD::MuonContainer::iterator muonSC_itr = (muons_shallowCopy.first)->begin();
  xAOD::MuonContainer::iterator muonSC_end = (muons_shallowCopy.first)->end();
  ngoodmu =0;
  int nsignalmu =0;
  bool muTrigMathched = 0;
  for( ; muonSC_itr != muonSC_end; ++muonSC_itr ) 
  {
    (*muonSC_itr)->auxdecor< bool >( "inrecoil") = false;
    if(m_muonCalibrationAndSmearingTool->applyCorrection(**muonSC_itr) == CP::CorrectionCode::Error)
    {// apply correction and check return code
      // Can have CorrectionCode values of Ok, OutOfValidityRange, or Error. Here only checking for Error.
      // If OutOfValidityRange is returned no modification is made and the original muon values are taken.
      Error("execute()", "MuonCalibrationAndSmearingTool returns Error CorrectionCode");
    }

    bool    mu_iso = m_isolationSelectionTool-> accept(**muonSC_itr);
    bool    mu_acc = m_muonSelection->accept(**muonSC_itr);
    bool    mu_acc_incl = m_muonInclSelection->accept(**muonSC_itr);

    const xAOD::TrackParticle *mutrk = (*muonSC_itr)->primaryTrackParticle();
    //if no track particle, skip event
    if(!mutrk) continue;
    float mu_d0sig = 10.,mu_deltaz0=10.,mu_deltaz0new=10.;
    if(pv && mutrk){
      mu_d0sig = xAOD::TrackingHelpers::d0significance( mutrk, eventInfo.beamPosSigmaX(), eventInfo.beamPosSigmaY(), eventInfo.beamPosSigmaXY() );
      mu_deltaz0 = fabs(mutrk->z0() + mutrk->vz() - pv->z());
      mu_deltaz0new = mu_deltaz0 * sin(mutrk->theta());
      (*muonSC_itr)-> auxdata<float>( "deltaz0new" ) = mu_deltaz0new;
      (*muonSC_itr)-> auxdata<float>( "d0sig" ) = mu_d0sig;
    }
    bool isMuVtxAssoc = (fabs(mu_d0sig)<3) && (fabs(mu_deltaz0new)<0.5); 
    // require : medium muon, pT > 5 Gev
    if(!mu_acc_incl || ((*muonSC_itr)->pt()<m_muonMinPtIncl))  continue;
    xAOD::Muon* inclMu = new xAOD::Muon();
    inclMu->makePrivateStore((**muonSC_itr));
    inclMuons->push_back(inclMu); 	
    //require : pT >20 GeV		
    if((*muonSC_itr)->pt()<m_muonMinPt) continue;
    //	if(! (fabs((*muonSC_itr)->eta())<m_muonMaxEta)) continue; 
    //check if it's tight
    (*muonSC_itr)-> auxdata<bool>( "isTight" ) = mu_acc;
    //require : vertex association
    if(!isMuVtxAssoc)   continue;
    if(mu_iso) {
      if((*muonSC_itr)->pt() > m_muonFinalMinPt && mu_acc) nsignalmu++;
      continue;
    }
    // do MJ, retrive d0/z0, ptvarcone, topoetcone	
    if((*muonSC_itr)->pt() > m_muonFinalMinPt && mu_acc) ngoodmu++;
    xAOD::Muon* tempMu = new xAOD::Muon();
    tempMu ->makePrivateStore((**muonSC_itr));
    bool  muTrMatch = 0;
    std::vector<const xAOD::IParticle*> myMuons(1,*muonSC_itr);
    for(TString str : muontrigmatchnames) 
    {
      //for non stdm4
      if(m_muTrigMatch->match( myMuons, str.Data() )) muTrMatch = 1;
      //for stdm4
      //TString trigname="STDM4_"+str;
      //if(tempMu->auxdata< char >( trigname.Data() ))muTrMatch = 1;
    }
    //done during gettrigger -- but works for STDM4, have to make a switch
    tempMu->auxdata< bool >( "trigMatched" ) = muTrMatch;
    // tempMu->auxdata< float >( "ptvarcone20" ) = i
    float sfiso = 1., sfreco_medium = 1. , sfreco_tight = 1., sfTTVA = 1., effiso = 1.;
    if(m_isMC){
      m_effi_medium_corr->getEfficiencyScaleFactor(**muonSC_itr, sfreco_medium);
      m_effi_tight_corr->getEfficiencyScaleFactor(**muonSC_itr, sfreco_tight);
      //std::cout << " print 1 " << sfreco << " " << sfiso << " " << sfTTVA << std::endl;
      m_iso_corr->getEfficiencyScaleFactor(**muonSC_itr, sfiso);
      //std::cout << " print 2 " << sfreco << " " << sfiso << " " << sfTTVA << std::endl;
      m_iso_corr->getDataEfficiency(**muonSC_itr, effiso);

      m_TTVA_effi_corr->getEfficiencyScaleFactor(**muonSC_itr, sfTTVA);
      //std::cout << " print 3 " << sfreco << " " << sfiso << " " << sfTTVA << std::endl;

      tempMu->auxdata< float >( "sfiso") = sfiso;
      tempMu->auxdata< float >( "sfTTVA") = sfTTVA;
      tempMu->auxdata< float >( "sfreco_medium") = sfreco_medium;
      tempMu->auxdata< float >( "sfreco_tight") = sfreco_tight;
      tempMu->auxdata< std::string >( "rg_sfreco_tight") = m_effi_tight_corr->getUncorrelatedSysBinName(m_effi_tight_corr->getUnCorrelatedSystBin(**muonSC_itr));
      tempMu->auxdata< std::string >( "rg_sfreco_medium") = m_effi_medium_corr->getUncorrelatedSysBinName(m_effi_medium_corr->getUnCorrelatedSystBin(**muonSC_itr));
      tempMu->auxdata< std::string >( "rg_sfTTVA") = m_TTVA_effi_corr->getUncorrelatedSysBinName(m_TTVA_effi_corr->getUnCorrelatedSystBin(**muonSC_itr));
      tempMu->auxdata< std::string >( "rg_sfiso") = m_iso_corr->getUncorrelatedSysBinName(m_iso_corr->getUnCorrelatedSystBin(**muonSC_itr));
    }
    tempMu->auxdata< float >( "deltaz0new" ) = (*muonSC_itr)-> auxdata<float>( "deltaz0new" );
    tempMu->auxdata< float >( "d0sig" ) = (*muonSC_itr)-> auxdata<float>( "d0sig" );
    tempMu->auxdata< float >( "ptvarcone30" ) = (*muonSC_itr)-> auxdata<float>( "ptvarcone30" );
    tempMu->auxdata< float >( "topoetcone20" ) = (*muonSC_itr)-> auxdata<float>( "topoetcone20" );
    mucont->push_back(tempMu);

    //Info("execute()", "  corrected muon pt = %.2f GeV", ((*muonSC_itr)->pt() * 0.001));  

  } // end for loop over shallow copied muons
  delete muons_shallowCopy.first;
  delete muons_shallowCopy.second;

  Double_t temp_sftrig = 1., temp_efftrig = 1.;
  //Compute the muon trigger SF according to the selection :
  //Z selection (2 muons)
  //W selection (use the muon that matched the trigger)

  if(m_isMC)
  {
    ConstDataVector<xAOD::MuonContainer> tempMuonContainer(SG::VIEW_ELEMENTS);
    std::string trigChain = m_muonMCTrigMatch.Data();
    for(const auto& mu : *mucont) 
    {
      temp_sftrig =1.;
      temp_efftrig = 1.;
      tempMuonContainer.push_back(mu);
      if(!m_do5TeV){
	m_muTrigEff->getTriggerScaleFactor(*(tempMuonContainer.asDataVector()), temp_sftrig, trigChain );
	m_muTrigEff->getTriggerEfficiency(*mu, temp_efftrig, trigChain, false);
      }
      tempMuonContainer.clear();
      mu->auxdata<float>("sftrig") = temp_sftrig;
      mu->auxdata<float>("trigeff") = temp_efftrig;
    }
  }

  //m_event.record( mucont, "mucont"  );m_event.record( inclMuons, "inclMuons"  );
  //maybe have to keep that (?)
  /*
     m_event.record( mucont, "mucont"  );
     m_event.record( mucontAux, "mucontAux."  );
     m_event.record( inclMuons, "inclMuons"  );
     m_event.record( inclMuonsAux, "inclMuonsAux."  );*/

  if(nsignalmu >0 ) { mucont->clear(); return 1;}
  if( inclMuons->size() <1 )
    return 1;
  if( ngoodmu ==0 )
    return 1;

  //std::sort(mucont->begin(),mucont->end(),DescendingPt());
  return 0;

}

void muons::InitialiseMuonTools(bool isMC) {
  m_isMC = isMC;
  if(m_isMC)
  {
    m_TTVA_effi_corr = new CP::MuonEfficiencyScaleFactors("TTVA_corr");
    m_TTVA_effi_corr->setProperty("WorkingPoint",m_MuToolTTVAWP.Data());
    m_TTVA_effi_corr->setProperty("CalibrationRelease",m_MuToolTTVACalib.Data());
    //m_TTVA_effi_corr->setProperty("PileupReweightingTool",m_prwtool);
    m_TTVA_effi_corr->setProperty("UncorrelateSystematics",true);
    if (! m_TTVA_effi_corr->initialize().isSuccess()){
      Error("initialize()", "Failed to properly initialize the TTVA Tool. Exiting." );
      exit(10);
    }


    // Muon identification Efficiency - medium
    m_effi_medium_corr = new CP::MuonEfficiencyScaleFactors("effi_corr_medium");
    m_effi_medium_corr->setProperty("OutputLevel", MSG::INFO);
    m_effi_medium_corr->setProperty("WorkingPoint","Medium");
    //m_effi_corr->setProperty("RunNumber",282625);
    m_effi_medium_corr->setProperty("CalibrationRelease",m_MuToolEffiCalib.Data());//Maybe have to follow up on it
    //m_effi_corr->setProperty("PileupReweightingTool",m_prwtool);
    m_effi_medium_corr->setProperty("UncorrelateSystematics",true);
    if (! m_effi_medium_corr->initialize().isSuccess() ){
      Error("initialize()", "Failed to properly initialize the MuonEfficiencyScaleFactors Tool. Exiting." );
      exit(10);
    }
    // Muon identification Efficiency - tight
    m_effi_tight_corr = new CP::MuonEfficiencyScaleFactors("effi_corr_tight");
    m_effi_tight_corr->setProperty("OutputLevel", MSG::INFO);
    m_effi_tight_corr->setProperty("WorkingPoint","Tight");
    //m_effi_corr->setProperty("RunNumber",282625);
    m_effi_tight_corr->setProperty("CalibrationRelease",m_MuToolEffiCalib.Data());//Maybe have to follow up on it
    //not needed in principle
    //m_effi_tight_corr->setProperty("UncorrelateSystematics",true);

    if (! m_effi_tight_corr->initialize().isSuccess() ){
      Error("initialize()", "Failed to properly initialize the MuonEfficiencyScaleFactors Tool. Exiting." );
      exit(10);
    }
    // Muon isolation Efficiency
    m_iso_corr = new CP::MuonEfficiencyScaleFactors("iso_corr");
    m_iso_corr->setProperty("OutputLevel", MSG::INFO);
    m_iso_corr->setProperty("WorkingPoint",m_MuToolIsoWP.Data());
    //m_iso_corr->setProperty("PileupReweightingTool",m_prwtool);
    m_iso_corr->setProperty("UncorrelateSystematics",true);
    if (! m_iso_corr->initialize().isSuccess() ){
      Error("initialize()", "Failed to properly initialize the MuonIsolation Tool. Exiting." );
      exit(10);
    }
    // Trig Muon Efficiency
    m_muTrigEff = new CP::MuonTriggerScaleFactors("muonTrigEff");
    m_muTrigEff->setProperty("OutputLevel", MSG::INFO);
    //m_muTrigEff->setProperty("Year",  m_MuToolTrigEffRunNum.Data());//Period D
    m_muTrigEff->setProperty("MuonQuality", m_MuToolTrigEffMuQual.Data());
    //m_muTrigEff->setProperty("CalibrationRelease", m_MuToolEffiCalib.Data());
    //m_muTrigEff->setProperty("CalibrationRelease", "170303_Moriond");
    //m_muTrigEff->setProperty("Isolation", "");
    //m_muTrigEff->setProperty("MC", "mc15c");
    m_muTrigEff->initialize();

  }
  // initialize the muon calibration and smearing tool
  m_muonCalibrationAndSmearingTool = new CP::MuonCalibrationAndSmearingTool( "MuonCorrectionTool" );
  m_muonCalibrationAndSmearingTool->setProperty("OutputLevel", MSG::INFO);
  m_muonCalibrationAndSmearingTool->setProperty("Year",m_MuToolCalibAndSmearYear.Data());
  m_muonCalibrationAndSmearingTool->setProperty("Algo",m_MuToolCalibAndSmearAlgo.Data());
  m_muonCalibrationAndSmearingTool->setProperty("SmearingType",m_MuToolCalibAndSmearType.Data());
  m_muonCalibrationAndSmearingTool->setProperty("StatComb",m_MuToolCalibAndSmearStatComb);
  m_muonCalibrationAndSmearingTool->setProperty("Release",m_MuToolCalibAndSmearRel.Data()); 
  m_muonCalibrationAndSmearingTool->setProperty("SagittaCorr", m_MuToolCalibAndSmearSagittaCorr); //apply sagitta correction
  m_muonCalibrationAndSmearingTool->setProperty("doSagittaMCDistortion", m_MuToolCalibAndSmearSagittaMCDist); 
  m_muonCalibrationAndSmearingTool->setProperty("SagittaRelease", m_MuToolCalibAndSmearSagittaRel.Data()); 

  //I don't touch the default values - not sure why this is seen in other codes
  //m_muonCalibrationAndSmearingTool->setProperty("Release","PreRecs");
  if (! m_muonCalibrationAndSmearingTool->initialize().isSuccess() ){
    Error("initialize()", "Failed to properly initialize the MuonCalibrationAndSmearingTool Tool. Exiting." );
    exit(10);
  }
  //Muon Selection Tool
  m_muonSelection = new CP::MuonSelectionTool("MuonSelection");
  m_muonSelection->setProperty("OutputLevel", MSG::INFO);
  //	const float muonMaxEta = m_muonMaxEta;

  //	m_muonSelection->setProperty("MaxEta", m_muonMaxEta);
  m_muonSelection->setProperty("MaxEta", 2.4);
  m_muonSelection->setProperty("MuQuality", m_muonQuality); //(0, 1, 2, 3) = Tight, Medium, Loose, VeryLoose

  m_muonInclSelection = new CP::MuonSelectionTool("MuonInclSelection");
  m_muonInclSelection->setProperty("OutputLevel", MSG::INFO);
  m_muonInclSelection->setProperty("MuQuality", 1);

  if (! m_muonSelection->initialize().isSuccess() ){
    Error("initialize()", "Failed to properly initialize the Muon Selection Tool. Exiting." );
    exit(10);
  }
  if (! m_muonInclSelection->initialize().isSuccess() ){
    Error("initialize()", "Failed to properly initialize the Muon Selection Tool. Exiting." );
    exit(10);
  }
  m_isolationSelectionTool = new CP::IsolationSelectionTool("mu_IsolationSelectionTool");
  m_isolationSelectionTool->setProperty("OutputLevel", MSG::INFO);
  m_isolationSelectionTool->setProperty("MuonWP", m_IsolToolMuonWP.Data());
  m_isolationSelectionTool->initialize();


}

#endif

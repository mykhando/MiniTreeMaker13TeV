#!/bin/bash
#you need to setup your environment variables first : inside build, source */setup.sh
USERNAME=fballi
DATE=`date --utc '+20%y%m%d.%H%M' | cut -d '.' -f 1`
BLACKLIST=

OUTPUTS=outTree.root


echo "change dir $TMPDIR"
cd $TMPDIR
# There are a few files to be copied and uploaded to the grid
#please adapt to your own convenience
#be careful with the paths indicated in the config file as they will read the GRL, lumi and PRW files from the current directory -- see exemple settings file in this current scripts directory
buildDir=x86_64-slc6-gcc62-opt
shareDir=/afs/in2p3.fr/home/f/fballi/private/workspace/w/rc/lowmu/analysis/source/MiniTreeMaker13TeV/share
lumiFile=$shareDir/ilumicalc_histograms_None_341294-341649_OflLumi-13TeV-001.root
#lumiFile=$shareDir/ilumicalc_histograms_None_340644-341184_OflLumi-5TeV-001.root
PRWFile=$shareDir/13tevPRW.root
GRLFile=$shareDir/data17_13TeV.periodN_DetStatus-v98-pro21-16_Unknown_PHYS_StandardGRL_All_Good_25ns_ignore_GLOBAL_LOWMU.xml
#GRLFile=$shareDir/data17_5TeV.periodAllYear_DetStatus-v98-pro21-16_Unknown_PHYS_StandardGRL_All_Good_25ns_ignore_GLOBAL_LOWMU.xml
configFile=/afs/in2p3.fr/home/f/fballi/private/workspace/w/rc/lowmu/analysis/scripts/settings.config_13TeV
config=settings.config_13TeV


cp $configFile .
cp -r ${UserAnalysis_DIR} .
cp $lumiFile $PRWFile $GRLFile $buildDir/. 

tar -cf tarballMini.tar $buildDir $config
cd -
cp $TMPDIR/tarballMini.tar .
#for i in $(cat list_13TeV_2015_highmu);
#for i in $(cat input_xAOD_mc16_5tev.txt);
for i in $(cat input);
#for i in $(cat input_xAOD_mc_TCscan.txt);
#for i in $(cat input_xAOD_data17_13tev.txt);
do
    export dsid=`echo $i | cut -d '.' -f 2`
    export mcname=`echo $i | cut -d '.' -f 3`
    #export dsid=`echo $i | cut -d '.' -f 4`
    export tagout=`echo $i | cut -d '.' -f 6 | cut -d '/' -f 1`
    echo "running over dsid $dsid"
    echo "with tag : $tagout"
    #OUTDS=mc16_13TeV.${dsid}.TreeForTCScanLowMu.${tagout}_v01
    #OUTDS=data17_5TeV.${dsid}.Tree2017lowmuJPSI.${tagout}_v04
    OUTDS=mc16_13TeV.${dsid}.${mcname}.Tree2017lowmu.${tagout}_v00
    INDS=$i

    echo "Submitting to "${INDS}
    echo "Output DS name: "user.${USERNAME}.${OUTDS}.${DATE}
#please adapt the exec according to your config file and buildDir names
    prun --inDS=${INDS}\
         --outDS=user.${USERNAME}.${OUTDS}.${DATE}\
         --outputs=${OUTPUTS}\
	 --noBuild\
	 --noCompile\
         --exec="tar xf tarballMini.tar; source setup.sh; source x86_64-slc6-gcc62-opt/setup.sh; x86_64-slc6-gcc62-opt/bin/runWZ -i %IN -c ${config};"\
         --extFile tarballMini.tar,setup.sh\
         --excludedSite=${BLACKLIST}\
         --nGBPerJob=15
	 #--nFiles=5\
	 #--express\
         #--mergeOutput

done


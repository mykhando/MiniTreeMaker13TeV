#/bin/bash

curpath=`readlink -f .`
workpath="${curpath}/Output"
queue="medium"
path_framework="/afs/in2p3.fr/home/f/fballi/private/workspace/w/rc/lowmu/analysis/build/"
#name of config file template inside the share directory
origConfig="ConfigContain/settings.config_13TeV.bk"
dirfinal="/sps/atlas/f/fballi/minitree_lowmu/wz"
if [ ! -d "$dirfinal" ]; then
  mkdir -p $dirfinal
fi
njob=3                    ### number of files run in one job

launchdir="/sps/atlas/f/fballi/launchLowMu"
ninput=0
ndone=0
jobnb=1
mkdir -p $workpath
mkdir -p $launchdir
for i in $(cat ./filepath.txt);
do
  [[ $i =~ "xroot" ]] && ispath=1
  [[ $i =~ "xroot" ]] || ispath=0
  if [ $ispath = 0 ]
  then
    if [ $ndone -lt $(( ninput-1 )) ];
    then
      subdir=$sampledir/dir_$(( ndone+1 ))_$((ninput-1))
      mkdir -p $subdir
      sed -e  "s#SAMPLEPATH#$filelist#g ; 5s,path_to_frame,$path_framework," $curpath/runjob.sh  > "$subdir/job_$(( ndone+1 ))_$((ninput-1)).sh"
      cd $subdir
      echo "now submiting job_$(( ndone+1 ))_$(( ninput-1 ))"
      finalpath=$sname/dir_$(( ndone+1 ))_$(( ninput-1 ))
      mkdir -p $dirfinal/$finalpath
      configfile=$path_framework/../source/MiniTreeMaker13TeV/share/ConfigContain/settings_${sname}_dir_$(( ndone+1 ))_$(( ninput-1 )).config
      sed -e "s#OUTDIR#$dirfinal/$finalpath#g" $path_framework/../source/MiniTreeMaker13TeV/share/${origConfig} > $configfile
      sed -i "s#CONFIG#$configfile#g" job_$(( ndone+1 ))_$((ninput-1)).sh
      #with queue specification
      #qsub -V -P P_atlas -cwd -q $queue -l xrootd=1 -l sps=1 job_$(( ndone+1 ))_$((ninput-1)).sh
      qsub -V -P P_atlas -cwd -l xrootd=1 -l sps=1 job_$(( ndone+1 ))_$((ninput-1)).sh
    fi		
    jobnb=1
    ndone=0
    ninput=1
    sampledir=$launchdir/$i
    mkdir -p $sampledir
    sname=`echo $i | awk -F ':' '{print $2}'`
    mkdir -p $dirfinal/$sname
    echo "submit sample:  $i"
  elif [ $jobnb -lt $njob ]
  then
    if [ $jobnb = 1 ]
    then
      filelist=$i
    else
      filelist="$filelist,$i"
    fi
    jobnb=$(( jobnb+1 ))
    ninput=$(( ninput+1 ))
  else
    filelist="$filelist,$i"
    subdir=$sampledir/dir_$(( ndone+1 ))_$ninput
    mkdir -p $subdir
    sed -e  "s#SAMPLEPATH#$filelist# ; 5s,path_to_frame,$path_framework," $curpath/runjob.sh  > "$subdir/job_$(( ndone+1 ))_$ninput.sh"
    cd $subdir
    echo "now submiting job_$(( ndone+1 ))_$ninput"

    finalpath=$sname/dir_$(( ndone+1 ))_$ninput
    mkdir -p $dirfinal/$finalpath
    configfile=$path_framework/../source/MiniTreeMaker13TeV/share/ConfigContain/settings_${sname}_dir_$(( ndone+1 ))_$((ninput)).config
    sed -e "s#OUTDIR#$dirfinal/$finalpath#g" $path_framework/../source/MiniTreeMaker13TeV/share/${origConfig} > $configfile
    sed -i "s#CONFIG#$configfile#g" job_$(( ndone+1 ))_$((ninput)).sh
    #with queue specification
    #qsub -V -P P_atlas -cwd -q $queue -l xrootd=1 -l sps=1  job_$(( ndone+1 ))_$((ninput)).sh
    qsub -V -P P_atlas -cwd -l xrootd=1 -l sps=1  job_$(( ndone+1 ))_$((ninput)).sh
    jobnb=1
    ndone=$ninput
    ninput=$(( ninput+1 ))
  fi
done

if [ $(( ndone+1 )) -le $(( ninput -1 )) ]
then
  subdir=$sampledir/dir_$(( ndone+1 ))_$((ninput-1))
  mkdir -p $subdir
  sed -e  "s#SAMPLEPATH#$filelist# ; 5s,path_to_frame,$path_framework," $curpath/runjob.sh  > "$subdir/job_$(( ndone+1 ))_$((ninput-1)).sh"
  cd $subdir
  echo "now submiting job_$(( ndone+1 ))_$(( ninput-1))"
  finalpath=$sname/dir_$(( ndone+1 ))_$(( ninput-1))
  mkdir -p $dirfinal/$finalpath
  #sed -e "s#OUTDIR#$dirfinal/$finalpath#g" $path_framework/../source/MiniTreeMaker13TeV/share/${origConfig} > $path_framework/../source/MiniTreeMaker13TeV/share/settings.config

  configfile=$path_framework/../source/MiniTreeMaker13TeV/share/ConfigContain/settings_${sname}_dir_$(( ndone+1 ))_$((ninput-1)).config
  sed -e "s#OUTDIR#$dirfinal/$finalpath#g" $path_framework/../source/MiniTreeMaker13TeV/share/${origConfig} > $configfile
  sed -i "s#CONFIG#$configfile#g" job_$(( ndone+1 ))_$((ninput-1)).sh
  #with queue specification
  #qsub -V -P P_atlas -cwd -q $queue -l xrootd=1 -l sps=1  job_$(( ndone+1 ))_$((ninput-1)).sh   qsub -V -P P_atlas -cwd -l xrootd=1 -l sps=1  job_$(( ndone+1 ))_$((ninput-1)).sh
fi


